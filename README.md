# Scenario 22.11

Implementation Scenario for the Gaia-X Summit 2022

# Architecture

* Description of the [components](components.md)
    * [Main Schema](components.md#functional-blocks-schema)
    * [Functional Blocks Description](components.md#functional-blocks-explanations)
    * [Usage and Workflow](components.md#components-usage)

# Input/Ouput description + sample demo example

Description of the input and output for each components with sample documents used during the demo [sample documents](sample documents.md)

# Deployement Architecture

Description of the namespace and container deployed to play the demo: [deployment](deployment.md)

# FAQ

Frequently asked questions: [FAQ](faq.md)
