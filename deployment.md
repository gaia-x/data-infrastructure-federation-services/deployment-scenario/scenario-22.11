# DRAFT Deployment: Organization - Systems - Functional blocks - Software Component

```mermaid
C4Deployment
    title Deployment Diagram for scenario 22.11

    Enterprise_Boundary(p_CA, "MontBlanc IOT"){
        Person(p_Alice,"Alice","BI Analyst of MontBlanc IOT")
        Person(p_Mary,"Mary","Admin of Mont Blanc IOT")
        System_Boundary(ns_MB,"Namespace: provider montblanc-iot"){
            Container(ca_VCI, "vc-issuer", "MontBlanc VC Issuer")
            Container(ca_DID, "didweb <br> https://montblanc-iot.gaia-x.community/.well-known/did.json")
            Container(ca_UAc, "participant-agent", "Participant Agent API")
        }
        System_Boundary(vm_TL,"VM: Teralab"){
            Container(ca_WW, "Web Wallet", "Alice Web Wallet")
        }      
    }


    Enterprise_Boundary(p_CB, "Dufour Storage"){
        Person(p_B,"Bob","VP Marketing Dufour Storage")
        System_Boundary(ns_B,"Namespace: Provider Dufour"){
            Container(cb_VCI, "vc-issuer", "Dufour Storage VC Issuer")
            Container(cb_DID, "didweb <br> https://dufourstorage.gaia-x.community/.well-known/did.json")
            Container(cb_UAc, "participant-agent", "Participant Agent API")
            Container(cb_sdui,"service-description-wizard-tool")
        }
        System_Boundary(cb_infra,"Enterprise PRODUCTION VM: Teralab"){
            Container(cb_Storage, "Minio Service")
            Container(cb_Bridge, "OpenId Bridge")
            Container(cb_PRC, "OPA")
        }
    }

    Enterprise_Boundary(p_GX, "Gaia-X"){
        System_Boundary(ns_GX,"Namespace: Gaia-X"){
            Container(gx_VCI, "vc-issuer", "Gaia-X VC Issuer")
            Container(gx_DID, "didweb:<br> https://gaia-x.community/.well-known/did.json")
            Container(gx_UAc, "participant-agent", "Participant Agent API")
        }
    }

    Enterprise_Boundary(p_ABC, "Federator - ABC FEDERATION"){
        System_Boundary(ns_ABC,"Namespace: abc-federation"){
            Container(abc_FAc, "participant-agent", "Participant Agent API")
            Container(abc_DID, "didweb <br> https://abc-federation.gaia-x.community/.well-known/did.json")
            Container(abc_PRC, 'rules-checker","Rules Checker Service")
            Container(abc_PRC, 'OPA","OPA")
            Container(abc_VCIc, "vc-issuer", "VC Issuer")
            Container(abc_UAc, "participant-agent", "Participant Agent API")
            Container(abc_spc,"schema-provider","Schema Provider")
            Container(abc_sv,"schema-validator","Schema Validator")
            Container(abc_sn,"notarization","Notarization")
            Container(abc_TAc,"trusted-registry", "Trust Anchor Service")
            Container(abc_cfsc, "wizard-federated-catalog", "Wizard Federated Catalogue")
            Container(abc_cfsc, "federated-catalog-api", "Federated Service Catalogue")
        }
    }

    Enterprise_Boundary(p_CAB, "CAB <unknown>"){
        System_Boundary(ns_CAB,"Namespace: <unknown>"){
            Container(cab_PA, "participant-agent", "Participant Agent API")
            Container(cab_DID, "didweb: <br> https://unknown.auditor.gaia-x.community/.well-known/did.json")
            Container(cab_VCI, "vc-issuer", "VC Issuer", "")
        }
    }


UpdateLayoutConfig($c4ShapeInRow="4", $c4BoundaryInRow="2")

```
