# Sample documents

[TOC]

## Participant Informations

### ABC-Federation

```json
{
    'designation': 'ABC Federation',
    'registrationNumber': '',
    'legalAddress': {
        'country-name': 'BE',
        'street-address': 'Avenue des Arts 6-9',
        'locality': 'Bruxelles',
        'postal-code': '1210'
    },
    'leiCode': '',
}
```

### Dufour Storage (aka company B)
https://dufourstorage.provider.gaia-x.community/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json

```json
{
    'designation': 'Dufour Storage',
    'registrationNumber': '123456789',
    'legalAddress': {
        'country-name': 'IT',
        'street-address': 'Via Europa',
        'locality': 'Milano',
        'postal-code': '20121'
    },
    'headQuarterAddress': {
        'country-name': 'IT',
        'street-address': 'Via Europa',
        'locality': 'Milano',
        'postal-code': '20121'
    },
    'leiCode': '',
    'termsAndConditions': 'https://dufourstorage.gaia-x.community/terms-and-conditions/'
}
```


Location 
- Austria, InnsBruck
- Italy, Milano
- France, Grenoble
- Germany, Munchen

Services
- Storage Service
- Identity Provider
- Backup Service


### Mont Blanc IOT (aka company A)
https://montblanc-iot.provider.gaia-x.community/participant/39100b2d1113ff9136dc29a65dcae51f48e73735df72e38c1c375f3c7a08ba87/data.json

```json
{
    designation': 'Mont Blanc IOT',
    'registrationNumber': '123456789',
    'legalAddress': {
        'country-name': 'FR',
        'street-address': 'Place de l'aiguille du midi',
        'locality': 'Chamonix',
        'postal-code': '74400'
    },
    'headQuarterAddress': {
        'country-name': 'FR',
        'street-address': 'Place de l'aiguille du midi',
        'locality': 'Chamonix',
        'postal-code': '74400'
    },

    'leiCode': '',
}
```


## Step01

### s1 - Participant Dufour Storage - Claims from Schema

Rule checker sample Provider wrong https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/schema-validator/-/blob/main/data/provider-wrong.json
Rule checker sample Provider ok https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/schema-validator/-/blob/main/data/provider-ok.json
Rule checker sample Provider ko https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/schema-validator/-/blob/main/data/provider-ko.json 

### s1 - Participant Dufour Storage - Set of claims

```json
{
        "type": "LegalPerson",
        "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
        "gx-participant:registrationNumber": {
            "gx-participant:registrationNumberType": "local",
            "gx-participant:registrationNumberNumber": "0762747721"
        },
        "gx-participant:headquarterAddress": {
            "gx-participant:addressCountryCode": "BE",
            "gx-participant:addressCode": "BE-BRU",
            "gx-participant:streetAddress": "Avenue des Arts 6-9",
            "gx-participant:postalCode": "1210"
        },
        "gx-participant:legalAddress": {
            "gx-participant:addressCountryCode": "BE",
            "gx-participant:addressCode": "BE-BRU",
            "gx-participant:streetAddress": "Avenue des Arts 6-9",
            "gx-participant:postalCode": "1210"
        }
    }

```

### s1 - Participant Dufour Storage - Signed VC

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
    "type": [
        "VerifiableCredential",
        "LegalPerson"
    ],
    "issuer": "did:web:dufourstorage.provider.gaia-x.community",
    "credentialSubject": {
        "type": "LegalPerson",
        "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
        "gx-participant:registrationNumber": {
            "gx-participant:registrationNumberType": "local",
            "gx-participant:registrationNumberNumber": "0762747721"
        },
        "gx-participant:headquarterAddress": {
            "gx-participant:addressCountryCode": "BE",
            "gx-participant:addressCode": "BE-BRU",
            "gx-participant:streetAddress": "Avenue des Arts 6-9",
            "gx-participant:postalCode": "1210"
        },
        "gx-participant:legalAddress": {
            "gx-participant:addressCountryCode": "BE",
            "gx-participant:addressCode": "BE-BRU",
            "gx-participant:streetAddress": "Avenue des Arts 6-9",
            "gx-participant:postalCode": "1210"
        }
    },
    "expirationDate": "2023-06-21T12:17:52.960Z",
    "issuanceDate": "2023-03-23T13:18:30.558262+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
        "created": "2023-03-23T13:18:30.558262+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..chIeli_9WmZpBZWHYecP24xxlK6KyvZkzH4AnMg61daFcRKUuL7waBjBYO_w74nwPpR6BODg68h2HZigpXVHxroFXOTC94K-DIVBIwdrj72lT21LnP405sHkW555beZl-KFoh23aXAjFHngJf84_aARgu7aJkuhYYr9fWYZ-MNi83p5NK74z6D0rj9b_8Aq3EAxIWP9UlEnI5_wS4cqqefStiXbuA14fQF3zjKIkDrmmN4lSFCqTcfR7_j1twal9vHS1ap23-TgqFRKDkFPepqcAi934aFYS16exJ7PJJoUWcS4MFwA54cLwNiWksrv7D1n0pATcAZdSbkjdomcfcw"
    }
}
```



### s1 - Participant Dufour Storage - Unsigned VP (Compliance input)

```json
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1"
  ],
  "@id": "did:web:abc-federation.gaia-x.community",
  "@type": [
    "verifiablePresentation"
  ],
  "verifiableCredential": [
   {
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
    "type": [
        "VerifiableCredential",
        "LegalPerson"
    ],
    "issuer": "did:web:dufourstorage.provider.gaia-x.community",
    "credentialSubject": {
        "type": "LegalPerson",
        "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
        "gx-participant:registrationNumber": {
            "gx-participant:registrationNumberType": "local",
            "gx-participant:registrationNumberNumber": "0762747721"
        },
        "gx-participant:headquarterAddress": {
            "gx-participant:addressCountryCode": "BE",
            "gx-participant:addressCode": "BE-BRU",
            "gx-participant:streetAddress": "Avenue des Arts 6-9",
            "gx-participant:postalCode": "1210"
        },
        "gx-participant:legalAddress": {
            "gx-participant:addressCountryCode": "BE",
            "gx-participant:addressCode": "BE-BRU",
            "gx-participant:streetAddress": "Avenue des Arts 6-9",
            "gx-participant:postalCode": "1210"
        }
    },
    "expirationDate": "2023-06-21T12:17:52.960Z",
    "issuanceDate": "2023-03-23T13:18:30.558262+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
        "created": "2023-03-23T13:18:30.558262+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..chIeli_9WmZpBZWHYecP24xxlK6KyvZkzH4AnMg61daFcRKUuL7waBjBYO_w74nwPpR6BODg68h2HZigpXVHxroFXOTC94K-DIVBIwdrj72lT21LnP405sHkW555beZl-KFoh23aXAjFHngJf84_aARgu7aJkuhYYr9fWYZ-MNi83p5NK74z6D0rj9b_8Aq3EAxIWP9UlEnI5_wS4cqqefStiXbuA14fQF3zjKIkDrmmN4lSFCqTcfR7_j1twal9vHS1ap23-TgqFRKDkFPepqcAi934aFYS16exJ7PJJoUWcS4MFwA54cLwNiWksrv7D1n0pATcAZdSbkjdomcfcw"
    }
}
  ]
}

```
### s1 - Participant Dufour Storage - Compliance Certification Credential (missing type at the moment)
```json
{
    "@context": [
      "https://www.w3.org/2018/credentials/v1"
    ],
    "type": [
      "VerifiableCredential",
      "ParticipantCredential"
    ],
    "id": "https://catalogue.gaia-x.eu/credentials/ParticipantCredential/1679577676463",
    "issuer": "did:web:compliance.abc-federation.gaia-x.community",
    "issuanceDate": "2023-03-23T13:21:16.463Z",
    "expirationDate": "2023-06-21T13:21:16.463Z",
    "credentialSubject": {
      "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
      "hash": "f91580378e8659e35d76f459b31934cea921f848bb75ae0299e6e9072f0b2fa9"
    },
    "proof": {
      "type": "JsonWebSignature2020",
      "created": "2023-03-23T13:21:16.463Z",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..GXT_UjizPkwS4NJaG4-ITz6Kzs3PLv4Qr-wLrsGZncC9whe8LDZEJH90Dj4c-VCl8wflISHoYVTID4yprZe9eaif8sDJUMx8tmcTrYeEUcvjzwPYAx8zh3wXtxEpiDGhgVuj2ROQjJqKzS4m8RoumKHuxG4EWvBKcS0UqYcj6uF4VG3a7mMiF1c4NZGQJvmlDCGTEt5LoEJhnHaMdGgCYdmMKpRJ7Ofa9f_ZlDPQLTJrlRM3Z-7DgkpEKdUsG_6C6DefurdS2Lx9YvdLzknzlRgrpB5LD16diIF-1b8cxTpq0BoC29AQVUEd8GO3j-2G9JlJwVQUYS2ncQ9LX0dKkQ",
      "verificationMethod": "did:web:compliance.abc-federation.gaia-x.community"
    }
  }
```

### s1 - Participant Dufour Storage - Participant Self-Description stored on user-agent (end of S1)
```json

{
  "selfDescriptionCredential":    {
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
    "type": [
        "VerifiableCredential",
        "LegalPerson"
    ],
    "issuer": "did:web:dufourstorage.provider.gaia-x.community",
    "credentialSubject": {
        "type": "LegalPerson",
        "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
        "gx-participant:registrationNumber": {
            "gx-participant:registrationNumberType": "local",
            "gx-participant:registrationNumberNumber": "0762747721"
        },
        "gx-participant:headquarterAddress": {
            "gx-participant:addressCountryCode": "BE",
            "gx-participant:addressCode": "BE-BRU",
            "gx-participant:streetAddress": "Avenue des Arts 6-9",
            "gx-participant:postalCode": "1210"
        },
        "gx-participant:legalAddress": {
            "gx-participant:addressCountryCode": "BE",
            "gx-participant:addressCode": "BE-BRU",
            "gx-participant:streetAddress": "Avenue des Arts 6-9",
            "gx-participant:postalCode": "1210"
        }
    },
    "expirationDate": "2023-06-21T12:17:52.960Z",
    "issuanceDate": "2023-03-23T13:18:30.558262+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
        "created": "2023-03-23T13:18:30.558262+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..chIeli_9WmZpBZWHYecP24xxlK6KyvZkzH4AnMg61daFcRKUuL7waBjBYO_w74nwPpR6BODg68h2HZigpXVHxroFXOTC94K-DIVBIwdrj72lT21LnP405sHkW555beZl-KFoh23aXAjFHngJf84_aARgu7aJkuhYYr9fWYZ-MNi83p5NK74z6D0rj9b_8Aq3EAxIWP9UlEnI5_wS4cqqefStiXbuA14fQF3zjKIkDrmmN4lSFCqTcfR7_j1twal9vHS1ap23-TgqFRKDkFPepqcAi934aFYS16exJ7PJJoUWcS4MFwA54cLwNiWksrv7D1n0pATcAZdSbkjdomcfcw"
    }
}

,
  "complianceCredential": {
    "@context": [
      "https://www.w3.org/2018/credentials/v1"
    ],
    "type": [
      "VerifiableCredential",
      "ParticipantCredential"
    ],
    "id": "https://catalogue.gaia-x.eu/credentials/ParticipantCredential/1679577676463",
    "issuer": "did:web:compliance.abc-federation.gaia-x.community",
    "issuanceDate": "2023-03-23T13:21:16.463Z",
    "expirationDate": "2023-06-21T13:21:16.463Z",
    "credentialSubject": {
      "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/data.json",
      "hash": "f91580378e8659e35d76f459b31934cea921f848bb75ae0299e6e9072f0b2fa9"
    },
    "proof": {
      "type": "JsonWebSignature2020",
      "created": "2023-03-23T13:21:16.463Z",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..GXT_UjizPkwS4NJaG4-ITz6Kzs3PLv4Qr-wLrsGZncC9whe8LDZEJH90Dj4c-VCl8wflISHoYVTID4yprZe9eaif8sDJUMx8tmcTrYeEUcvjzwPYAx8zh3wXtxEpiDGhgVuj2ROQjJqKzS4m8RoumKHuxG4EWvBKcS0UqYcj6uF4VG3a7mMiF1c4NZGQJvmlDCGTEt5LoEJhnHaMdGgCYdmMKpRJ7Ofa9f_ZlDPQLTJrlRM3Z-7DgkpEKdUsG_6C6DefurdS2Lx9YvdLzknzlRgrpB5LD16diIF-1b8cxTpq0BoC29AQVUEd8GO3j-2G9JlJwVQUYS2ncQ9LX0dKkQ",
      "verificationMethod": "did:web:compliance.abc-federation.gaia-x.community"
    }
  }

}

```

## Step02

### s2 - Participant Dufour Storage - Service Offering Storage - SO Claims from Schema

Sample VC https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/policy-rules-provider/-/blob/main/VCExample/location.json

### s2 - Participant Dufour Storage - Service Offering Storage - Set of claims
```json
 {
        "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
        "gx-service-offering:providedBy": "https://docaposte.provider.gaia-x.community/participant/44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5be831bb07d4f260f/compliance-certificate-claim/vc/data.json",
        "gx-terms-and-conditions:serviceTermsAndConditions": {
          "gx-terms-and-conditions:value": "https://compliance.lab.gaia-x.eu/development",
          "gx-terms-and-conditions:hash": "myrandomhash"
        },
        "gx-service-offering:title": "DufourServiceTest",
        "gx-service-offering:serviceType": [
          "Authentication",
          "Storage"
        ],
        "gx-service-offering:description": "Teste",
        "gx-service-offering:descriptionMarkDown": "",
        "gx-service-offering:webAddress": "",
        "gx-service-offering:dataProtectionRegime": "",
        "gx-service-offering:dataExport": [
          {
            "gx-service-offering:requestType": "email",
            "gx-service-offering:accessType": "digital",
            "gx-service-offering:formatType": "mime/png"
          }
        ],
        "gx-service-offering:dependsOn": "",
        "gx-service-offering:keyword": "",
        "gx-service-offering:layer": "IAAS",
        "gx-service-offering:isAvailableOn": [
          "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
        ]
      }
```

```json
{
                "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/3a748292f6a45fec549803534f85b55e426fbbf85e14f8954fdd7d04d789d22f/data.json",
                "gx-service-offering:providedBy": "https://dufourstorage.provider.dev.gaiax.ovh/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/ce179ab2440f4b58bad8ae49666e09a0/data.json",
                "gx-terms-and-conditions:serviceTermsAndConditions": {
                    "gx-terms-and-conditions:value": "https://compliance.lab.gaia-x.eu/development",
                    "gx-terms-and-conditions:hash": "myrandomhash"
                },
                "gx-service-offering:title": "DufourServiceH",
                "gx-service-offering:serviceType": [
                    "Networking"
                ],
                "gx-service-offering:description": "Description",
                "gx-service-offering:descriptionMarkDown": "",
                "gx-service-offering:webAddress": "",
                "gx-service-offering:dataProtectionRegime": "",
                "gx-service-offering:dataExport": [
                    {
                        "gx-service-offering:requestType": "email",
                        "gx-service-offering:accessType": "digital",
                        "gx-service-offering:formatType": "mime/png"
                    }
                ],
                "gx-service-offering:dependsOn": "",
                "gx-service-offering:keyword": "",
                "gx-service-offering:layer": "PAAS",
                "gx-service-offering:isAvailableOn": [
                    "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
                ]
            }
```

### s2 - Participant Dufour Storage - Service Offering Storage - SO Signed VC
```json
{
   "@context":[
      "https://www.w3.org/2018/credentials/v1"
   ],
   "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
   "type":[
      "VerifiableCredential",
      "ServiceOfferingExperimental"
   ],
   "issuer":"did:web:dufourstorage.provider.gaia-x.community",
   "expirationDate":"2023-06-21T15:31:47.87+02:00",
   "credentialSubject":{
      "id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
      "gx-service-offering:providedBy":"https://docaposte.provider.gaia-x.community/participant/44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5be831bb07d4f260f/compliance-certificate-claim/vc/data.json",
      "gx-terms-and-conditions:serviceTermsAndConditions":{
         "gx-terms-and-conditions:value":"https://compliance.lab.gaia-x.eu/development",
         "gx-terms-and-conditions:hash":"myrandomhash"
      },
      "gx-service-offering:title":"DufourServiceTest",
      "gx-service-offering:serviceType":[
         "Authentication",
         "Storage"
      ],
      "gx-service-offering:description":"Teste",
      "gx-service-offering:descriptionMarkDown":"",
      "gx-service-offering:webAddress":"",
      "gx-service-offering:dataProtectionRegime":"",
      "gx-service-offering:dataExport":[
         {
            "gx-service-offering:requestType":"email",
            "gx-service-offering:accessType":"digital",
            "gx-service-offering:formatType":"mime/png"
         }
      ],
      "gx-service-offering:dependsOn":"",
      "gx-service-offering:keyword":"",
      "gx-service-offering:layer":"IAAS",
      "gx-service-offering:isAvailableOn":[
         "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
      ]
   },
   "issuanceDate":"2023-03-23T13:51:15.713775+00:00",
   "proof":{
      "type":"JsonWebSignature2020",
      "proofPurpose":"assertionMethod",
      "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
      "created":"2023-03-23T13:51:15.713775+00:00",
      "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..S_EUSotspBCjzY1IS3BCyAilADTxyft-l0FIsWkO99Ey_8bVYUOd76AARaY7Ru5fj7COb3_pc9SDYEoWyTmt6nOakPLWVxxQRZn-8wh-J2kbJDr4kaaFUYodXqk8NI4-5-ZiSGrPG_hx00BI4CGN1MtD6h3zml-xCDHq4RzEPSq6JpVYL7KGLjk4eHvYfyEbEbmwICZuuodgwbQPQ2DK6aU97IpmaNqXICNZZxbrZF_L5trr2lV-xGpdwxrpCcCoeEE15DAD1gEsDaune80p3uQdUvxmCuX9hpQ4SSjodATLJDAgsKeguU-TcRC7lWnjbm1t692owkzf-PIvVhmqeQ"
   }
}
```

```json
{
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "@id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/3a748292f6a45fec549803534f85b55e426fbbf85e14f8954fdd7d04d789d22f/data.json",
            "type": [
                "VerifiableCredential",
                "ServiceOfferingExperimental"
            ],
            "issuer": "did:web:dufourstorage.provider.gaia-x.community",
            "expirationDate": "2023-06-20T11:55:33.688+02:00",
            "credentialSubject": {
                "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/3a748292f6a45fec549803534f85b55e426fbbf85e14f8954fdd7d04d789d22f/data.json",
                "gx-service-offering:providedBy": "https://dufourstorage.provider.dev.gaiax.ovh/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/ce179ab2440f4b58bad8ae49666e09a0/data.json",
                "gx-terms-and-conditions:serviceTermsAndConditions": {
                    "gx-terms-and-conditions:value": "https://compliance.lab.gaia-x.eu/development",
                    "gx-terms-and-conditions:hash": "myrandomhash"
                },
                "gx-service-offering:title": "DufourServiceH",
                "gx-service-offering:serviceType": [
                    "Networking"
                ],
                "gx-service-offering:description": "Description",
                "gx-service-offering:descriptionMarkDown": "",
                "gx-service-offering:webAddress": "",
                "gx-service-offering:dataProtectionRegime": "",
                "gx-service-offering:dataExport": [
                    {
                        "gx-service-offering:requestType": "email",
                        "gx-service-offering:accessType": "digital",
                        "gx-service-offering:formatType": "mime/png"
                    }
                ],
                "gx-service-offering:dependsOn": "",
                "gx-service-offering:keyword": "",
                "gx-service-offering:layer": "PAAS",
                "gx-service-offering:isAvailableOn": [
                    "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
                ]
            },
            "issuanceDate": "2023-03-22T09:57:10.330600+00:00",
            "proof": {
                "type": "JsonWebSignature2020",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
                "created": "2023-03-22T09:57:10.330600+00:00",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..fgB-VcQ8y23HJBiYLb98ZBu8OLpqZyqW57UuFmFBtLXeZ4F5pWPXiAehsjiOkiDg4yLNcbEd6ry-zvKTIHaRGw1VK22qSNh48FF5U7tu-N72uXDN1cIIEqK_9XsbaLRn1TcV-aTdcCQmu7t8oUWKXtslsmCP9BXNtNtur4oLA0edLM8rulCV6-649J0Z02qGWbQsBXWuV17saIYKyBPtQyMc8ZC7yJ10VhvoUw3-hiKYybk2XxSnqQZ0YWzXyyZG6LnQ7tkIQIEfFKupg-DVB1P-_cLjNoH5QT5O77NaoCnSwU3HrQZbMMsKsaIu2nXJPTR1lqY9rPya8jvhqOYipw"
            }
        }

``` 

### s2 - Participant Dufour Storage - Service Offering Storage - Certification Claims - ISO27001

PDF sample: https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/notarization-service/-/blob/main/code/api-notarization/pdf_examples/signedByEIDAS.pdf 
https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/notarization-service/-/blob/main/code/api-notarization/pdf_examples/signedByAtosUser.pdf 
https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/notarization-service/-/blob/main/code/api-notarization/pdf_examples/signed-sample.pdf


### s2 - Participant Dufour Storage - Service Offering Storage - ThirdPartyComplianceCertificateCredential for complianceReference object - ISO27001

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-credential/45f423b5e583617ee137e0f891e648b31e3187635e983db4bbe0cbb185b3a8b0/data.json",
    "issuer": "did:web:abc-federation.gaia-x.community",
    "@type": [
        "VerifiableCredential",
        "ThirdPartyComplianceCertificateCredential"
    ],
    "credentialSubject": {
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-claim/be703233fc7f74669e7280d9c7e73cc8316047acd1a67cd4b28086eca422d716/data.json",
        "@type": "gax-compliance:ThirdPartyComplianceCertificateClaim",
        "@context": {
            "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
            "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#"
        },
        "isValid": {
            "@value": "True"
        }
    },
    "issuanceDate": "2023-03-29T08:11:21.250653+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2023-03-29T08:11:21.250653+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..UjaYk256UOlxN7iV_fsTdnIwN99DP1CPNIa366mmUl6_42odzg6ALfjAIUIewIu3qUY3wws-iuUwes04-PHGz-o9NOe8MJUcTQ8SKQf_7A6WDJ4sYENSd_3tWnRpbxzfAYQn54SgVlrOgvQg3GLSDsitJEcE5A2HyjrQrcK-OhQHMXnjncrC5ot0o7vIach7mTnAM45JS_hoYj4ABpFMTdD0byfVcihI-Kg7KRwVBUhYECszgYlEns4XWYHYU8L2gVeRuKa1AUJZj4c8NF8M4FtK7T9rqFjb0yWDe8DXN6bnJ1fdim4x0XfVY1x5af9XoMts-zlXtGwbOqLRWRz8QQ"
    }
}
```

### s2 - Participant Dufour Storage - Service Offering Storage - Certification Claims - SecNumCloud

```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-claim/20094b42876a335af0d5460545c27ad2c475fabd1cdaf2d41cd5d7f6767f3690/data.json",
    "issuer": "did:web:abc-federation.gaia-x.community",
    "@type": [
        "VerifiableCredential",
        "ThirdPartyComplianceCertificateClaim"
    ],
    "credentialSubject": {
        "@id": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certificate-claim/20094b42876a335af0d5460545c27ad2c475fabd1cdaf2d41cd5d7f6767f3690/data.json",
        "@type": "ThirdPartyComplianceCertificateClaim",
        "@context": {
            "gax-compliance": "https://schemas.abc-federation.gaia-x.community/wip/vocab/compliance#",
            "gax-participant": "https://schemas.abc-federation.gaia-x.community/wip/vocab/participant#",
            "gax-service": "https://schemas.abc-federation.gaia-x.community/wip/vocab/service#"
        },
        "gax-compliance:hasComplianceCertificationScheme": {
            "@type": "gax-compliance:ComplianceCertificationScheme",
            "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/third-party-compliance-certification-scheme/97374a5d3de620b61331ef705890e3635193afee721d2f74f6b024365c4efda7/data.json"
        },
        "gax-compliance:hasLocatedServiceOffering": {
            "@type": "gax-service:LocatedServiceOffering",
            "@value": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/located-service-offering/e4bfc6039e72e4c40120f581f96bf4d0ead23b8056d5288129751af76ddd5f45/data.json"
        },
        "gax-compliance:hasComplianceAssessmentBody": {
            "@type": "gax-participant:ComplianceAssessmentBody",
            "@value": "did:web:abc-federation.gaia-x.community:participant:8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/compliance-assessment-body/8121245b25bda4bd0eac915fc50319f9752d6d2e9eb4a16c7bf76dcc67c0eda3/data.json"
        }
    },
    "issuanceDate": "2023-03-29T08:41:55.343717+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:abc-federation.gaia-x.community",
        "created": "2023-03-29T08:41:55.343717+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..kVG6CTud375dSpdLY4AhAT3JAom-_bhszJLzXmSdrznZ0QPAi3pzUS_QXCIfMGDMFj6LCtA5JeK-Zzx389p9R1s9-vgCvzNYIQdl1KXY4IBmqTTKo-4IqFVZS9Nm0bfWSPE7X-zE3ecFK90pqoXly_sH1yGy2sfByL2acRrq181WfAOh3UkhcLWr49Ynlg3jbn4cxUET8lB4Vesg23EbRnGqgRf88l63o--JInU-7N279iD-2ir6lJ0SQuDwcvWnoEKkMcEIf8oEm98QKBRw_IcoYsQvGBzv6WXUlkoeJR7BtZhz_Ir2WooNwcysj8dHE9IT30gIq1yxMeGK8LltXA"
    }
}
```

### s2 - Participant Dufour Storage - Service Offering Storage - ThirdPartyComplianceCertificateCredential for complianceReference object - SecNumCloud


### s2 - Participant Dufour Storage - Service Offering Storage - SO VP Set of claims
```json
[
    {
   "@context":[
      "https://www.w3.org/2018/credentials/v1"
   ],
   "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
   "type":[
      "VerifiableCredential",
      "ServiceOfferingExperimental"
   ],
   "issuer":"did:web:dufourstorage.provider.gaia-x.community",
   "expirationDate":"2023-06-21T15:31:47.87+02:00",
   "credentialSubject":{
      "id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
      "gx-service-offering:providedBy":"https://docaposte.provider.gaia-x.community/participant/44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5be831bb07d4f260f/compliance-certificate-claim/vc/data.json",
      "gx-terms-and-conditions:serviceTermsAndConditions":{
         "gx-terms-and-conditions:value":"https://compliance.lab.gaia-x.eu/development",
         "gx-terms-and-conditions:hash":"myrandomhash"
      },
      "gx-service-offering:title":"DufourServiceTest",
      "gx-service-offering:serviceType":[
         "Authentication",
         "Storage"
      ],
      "gx-service-offering:description":"Teste",
      "gx-service-offering:descriptionMarkDown":"",
      "gx-service-offering:webAddress":"",
      "gx-service-offering:dataProtectionRegime":"",
      "gx-service-offering:dataExport":[
         {
            "gx-service-offering:requestType":"email",
            "gx-service-offering:accessType":"digital",
            "gx-service-offering:formatType":"mime/png"
         }
      ],
      "gx-service-offering:dependsOn":"",
      "gx-service-offering:keyword":"",
      "gx-service-offering:layer":"IAAS",
      "gx-service-offering:isAvailableOn":[
         "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
      ]
   },
   "issuanceDate":"2023-03-23T13:51:15.713775+00:00",
   "proof":{
      "type":"JsonWebSignature2020",
      "proofPurpose":"assertionMethod",
      "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
      "created":"2023-03-23T13:51:15.713775+00:00",
      "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..S_EUSotspBCjzY1IS3BCyAilADTxyft-l0FIsWkO99Ey_8bVYUOd76AARaY7Ru5fj7COb3_pc9SDYEoWyTmt6nOakPLWVxxQRZn-8wh-J2kbJDr4kaaFUYodXqk8NI4-5-ZiSGrPG_hx00BI4CGN1MtD6h3zml-xCDHq4RzEPSq6JpVYL7KGLjk4eHvYfyEbEbmwICZuuodgwbQPQ2DK6aU97IpmaNqXICNZZxbrZF_L5trr2lV-xGpdwxrpCcCoeEE15DAD1gEsDaune80p3uQdUvxmCuX9hpQ4SSjodATLJDAgsKeguU-TcRC7lWnjbm1t692owkzf-PIvVhmqeQ"
   }
},
    {
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "@id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/077849c113d60b532c273d132c0443e0c8604a71157ae1348e42e68153971e5e/data.json",
    "type": [
        "VerifiableCredential",
        "SelfAssessedComplianceCriteriaCredential"
    ],
    "issuer": "did:web:dufourstorage.provider.gaia-x.community",
    "expirationDate": "2023-06-21T15:31:47.87+02:00",
    "credentialSubject": {
        "grantsComplianceCriteriaCombination": [
            "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json",
            "did:web:gaia-x.community/compliance-criterion/ea307320071d2c97ba1fc264ac2b57fd571a30fd4bc08b85139ca3256a726b97/data.json",
            "did:web:gaia-x.community/compliance-criterion/2ee23dc8f147f6e8e5707f92161b2695d25c2571af06f832d55eb3dcd01109ca/data.json",
            "did:web:gaia-x.community/compliance-criterion/362213edf62b155d61e6d72c0dde5d7a01b982218eb5a2b00e7b1bb6015dee98/data.json",
            "did:web:gaia-x.community/compliance-criterion/1f2872e3642e6d616bf6af0a3f4c14ef6ca6111e6854184cfbf3cb4ef88a15d5/data.json",
            "did:web:gaia-x.community/compliance-criterion/1df51e3055940bad2b48205d85db6fc7968af811ce74a9383a99dd9bef548340/data.json",
            "did:web:gaia-x.community/compliance-criterion/c754817b22a03a60efc60569ef8cf483fbf0df402d1f72c863967ab393e9b3af/data.json",
            "did:web:gaia-x.community/compliance-criterion/10e1999e6107d991f3251a57252ca95b7564d7817f34ffcdb2520a1939746749/data.json",
            "did:web:gaia-x.community/compliance-criterion/3cb671f9bb51069b27ff31208025553f6386e7d37a7429dd058a9b5924fe64fc/data.json",
            "did:web:gaia-x.community/compliance-criterion/958b517ec144386f06c238dfafa801a4e719807da0a261ecdb1546cd1246a16c/data.json",
            "did:web:gaia-x.community/compliance-criterion/5c6c37938ccec54ddbe569d1e429658d7c3a86b3cb91f5d7b9fa81b61efe6eba/data.json",
            "did:web:gaia-x.community/compliance-criterion/9975196b786b14f3992114e5e1f1cd35befdb5e7fa439e2f94bef927f328ac49/data.json",
            "did:web:gaia-x.community/compliance-criterion/18e49d1e6c56efb00f43a863c87d147e8598a450a683ab6e0db245b7d4d2af86/data.json",
            "did:web:gaia-x.community/compliance-criterion/471b984f28f8a79f7c11f3e44fe298aab7152efc5f97d29fea6d45f2e9a0efd1/data.json",
            "did:web:gaia-x.community/compliance-criterion/98d0eec88ce471c8a0485636b95b7b5f28a743131e84c79a937e8c92de7e330c/data.json",
            "did:web:gaia-x.community/compliance-criterion/c51a27275cbb87b247d21b81ef16f585d114c22e48913eb6d9fb4f0e3aa01ba6/data.json",
            "did:web:gaia-x.community/compliance-criterion/69e118feec07463d4e827aa345ee659ce32e5075e6ddcf8daff7027a99f8893c/data.json",
            "did:web:gaia-x.community/compliance-criterion/25d78bede67bb195d69b9419808d74015c3d5f4a2d733c676a7244b3950ce1b9/data.json",
            "did:web:gaia-x.community/compliance-criterion/843d0a4650499b09e37b5dd7759da4ddbc4f7d7098bc806149a8829f9ea0f315/data.json",
            "did:web:gaia-x.community/compliance-criterion/36c42f80bc75fb96d66f64f1ae4f75798c7906f2a515adfae982367d3ace56dd/data.json",
            "did:web:gaia-x.community/compliance-criterion/c9280867077e758137a1b6ce8010698e187a98e1ebf01fc28e64be76611528ad/data.json",
            "did:web:gaia-x.community/compliance-criterion/53ff2d76126a8b7404fc769734043b3a24d5e7422029cb2666f2b8a4a11fb49b/data.json",
            "did:web:gaia-x.community/compliance-criterion/12f11e3c592b4285d3f051f933a99517b997a543d5705d8717a40af6bc150b2a/data.json",
            "did:web:gaia-x.community/compliance-criterion/e06d36d7b023d7b1da6ef0c9f8d451b8fb1ddecd4538e03d9c63fa3d1c59d990/data.json",
            "did:web:gaia-x.community/compliance-criterion/74b947356bc21c2daaf7f0984b53a1493ed4678f53680d63830842bd22fe352f/data.json",
            "did:web:gaia-x.community/compliance-criterion/f36bebe520dbf1736c3840769828d093791053c5aa6c6a46d1848595e00140bc/data.json",
            "did:web:gaia-x.community/compliance-criterion/9e45260877e1c896508618cefd9f16c185e72dd89fba426eb3901eff167d2360/data.json",
            "did:web:gaia-x.community/compliance-criterion/d604a332d9ca9c8f2b93fc779b35ec2e57818fdedba5d8cef260e69af524b8f2/data.json",
            "did:web:gaia-x.community/compliance-criterion/db57c2316ae02bfb8c4b66368735234fb917995b91123775b164cbbbfa19ca6f/data.json",
            "did:web:gaia-x.community/compliance-criterion/7bedfaaa1ed74be7de48eafac21cfbad4856c424f1638d1af0a2053d2cb227a9/data.json",
            "did:web:gaia-x.community/compliance-criterion/85044fd562ef04649b9d5530e7f4ef248ae6ab81b3c9fa1f4fcd3847ce1c94f1/data.json",
            "did:web:gaia-x.community/compliance-criterion/fb797087b3951edc3add77837cd5401cacc7f2fc206413ff3c9cbff563dd8f68/data.json",
            "did:web:gaia-x.community/compliance-criterion/cde1519cd7f68cce2951987c72d75decf3626a403e89d458fbceb7ac46f06f5a/data.json",
            "did:web:gaia-x.community/compliance-criterion/c4a921a3a3df7fa678c0a521a22a5dac0528bed85233719f71c7cbf52edf302a/data.json",
            "did:web:gaia-x.community/compliance-criterion/6dcd080420ba9a1f7db82682395ed54aacdda5b6ba7c001eec61e2d19226dd2a/data.json",
            "did:web:gaia-x.community/compliance-criterion/a1f306c65a2969c1233cb0142034c0ab21dcac4615f21ad45cba9a8e4e273653/data.json",
            "did:web:gaia-x.community/compliance-criterion/273a31001e5ea054732d0983c44343612a339b6c5e9c5eb2b1209d77487ad94e/data.json",
            "did:web:gaia-x.community/compliance-criterion/1d0535639c6c2e26790ac19a790ad547ac1b4d7c12bdd0f8dd4c3f8c726fc7e5/data.json",
            "did:web:gaia-x.community/compliance-criterion/f5284af5d5bf3b1a80a90c65f65f3ff15fa15ca524085685bae85ca135d62782/data.json",
            "did:web:gaia-x.community/compliance-criterion/733aca07e2d2e11cba61dc74e387d6ca289ee895e521c9c091141be15118c5ba/data.json",
            "did:web:gaia-x.community/compliance-criterion/75571bb4c63239f0df7168e1a44ebdf278fd37738eff607cfb9049c7c2bd083b/data.json",
            "did:web:gaia-x.community/compliance-criterion/cf15736ee674b6fbe9f32bc91a9a81a2b1b1135517a352037edfb0808cddba81/data.json",
            "did:web:gaia-x.community/compliance-criterion/f223175e85cf168bd7df2858c32b5db5267772986299221f4bba5762bd8482c8/data.json",
            "did:web:gaia-x.community/compliance-criterion/3c6be9b458b4402c708733302056fd55aa58d6f1e865b61b3bf03f3db6145e47/data.json",
            "did:web:gaia-x.community/compliance-criterion/b388093c6117171072423b78a1d223caeb66d79266301e695df169d1ab391c79/data.json",
            "did:web:gaia-x.community/compliance-criterion/d6c80de92f9426e57b637f56000530e5142a085abd704e63d708ad9eb68db5dc/data.json",
            "did:web:gaia-x.community/compliance-criterion/ed6a2f7f46f6faacf8142fda2733383ae6b268ccbdadf3293fa4eeb35daf8091/data.json",
            "did:web:gaia-x.community/compliance-criterion/33ffebcc9d1c490ddee3c16f039f801a035a8fe18f88262a7fd2d6b517b4c39e/data.json",
            "did:web:gaia-x.community/compliance-criterion/26c3d521a326ee292fd20ce72f3dfffe0ffd290769283f2a2a7738e8a6847d40/data.json",
            "did:web:gaia-x.community/compliance-criterion/bcc7f7d1ca3d08cc612e3fb3aacc4e3e9e57fd4f15fd6610fc1027f3cfd7ab10/data.json",
            "did:web:gaia-x.community/compliance-criterion/dfb922e542fe48da698dfd1599053f203311ca650003a786e88cc392ae54b698/data.json",
            "did:web:gaia-x.community/compliance-criterion/2808237322eab75f3d041469954dc61bcaab3153b63558d7f8f4449c4bfcc894/data.json",
            "did:web:gaia-x.community/compliance-criterion/bc3a08b25f3e84f6e0bbc06e03b038cc04c9d3eee2a654d956e98f6360e6baa0/data.json"
        ]
    },
    "issuanceDate": "2023-03-23T13:37:05.126757+00:00",
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
        "created": "2023-03-23T13:37:05.126757+00:00",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..K_IPBOEMUiClEjdCzaR0F83C_pSGRR24fNIT69-ZlBbuyzhutZiFTRBwd7zBUmICi_d8xMf3wp8aQQcypfekCU1uUXoGtlrxuc3cvhh4tO38_95zCiOEdyJ8_ZMv2QfyyxCv6OCH-AtSzkKVQ0Gdx_r7myVnN6mP5oftyDId0QJM6_GjRdyQxUM5UXZofHZ9b-OAyGfiM9R1ayBf7r2lwYNZNd8vmN67O_AFMnEkTgG9DhG42TwdVvpgyD002TaXrT8fo05ISx5cSGP5KVJ0yqe6eDx0k8e3OIlyrIC8R-wy6C9NJOkn_g1LJ0-vCOfJvc9rHvAg74QDkHKcW_yKkw"
    }
	}
  ]
```

### s2 - Participant Dufour Storage - Service Offering Storage - Signed SO VP
```json
{
   "@context":[
      "https://www.w3.org/2018/credentials/v1"
   ],
   "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vp/1cf8ad940dcc7215b34a82c37873cf864ad5b00173c1c275e1354a637660d965/data.json",
   "@type":[
      "verifiablePresentation"
   ],
   "expirationDate":"2023-06-21T15:31:47.87+02:00",
   "verifiableCredential":[
      {
         "@context":[
            "https://www.w3.org/2018/credentials/v1"
         ],
         "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
         "type":[
            "VerifiableCredential",
            "ServiceOfferingExperimental"
         ],
         "issuer":"did:web:dufourstorage.provider.gaia-x.community",
         "expirationDate":"2023-06-21T15:31:47.87+02:00",
         "credentialSubject":{
            "id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
            "gx-service-offering:providedBy":"https://docaposte.provider.gaia-x.community/participant/44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5be831bb07d4f260f/compliance-certificate-claim/vc/data.json",
            "gx-terms-and-conditions:serviceTermsAndConditions":{
               "gx-terms-and-conditions:value":"https://compliance.lab.gaia-x.eu/development",
               "gx-terms-and-conditions:hash":"myrandomhash"
            },
            "gx-service-offering:title":"DufourServiceTest",
            "gx-service-offering:serviceType":[
               "Authentication",
               "Storage"
            ],
            "gx-service-offering:description":"Teste",
            "gx-service-offering:descriptionMarkDown":"",
            "gx-service-offering:webAddress":"",
            "gx-service-offering:dataProtectionRegime":"",
            "gx-service-offering:dataExport":[
               {
                  "gx-service-offering:requestType":"email",
                  "gx-service-offering:accessType":"digital",
                  "gx-service-offering:formatType":"mime/png"
               }
            ],
            "gx-service-offering:dependsOn":"",
            "gx-service-offering:keyword":"",
            "gx-service-offering:layer":"IAAS",
            "gx-service-offering:isAvailableOn":[
               "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
            ]
         },
         "issuanceDate":"2023-03-23T13:51:15.713775+00:00",
         "proof":{
            "type":"JsonWebSignature2020",
            "proofPurpose":"assertionMethod",
            "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
            "created":"2023-03-23T13:51:15.713775+00:00",
            "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..S_EUSotspBCjzY1IS3BCyAilADTxyft-l0FIsWkO99Ey_8bVYUOd76AARaY7Ru5fj7COb3_pc9SDYEoWyTmt6nOakPLWVxxQRZn-8wh-J2kbJDr4kaaFUYodXqk8NI4-5-ZiSGrPG_hx00BI4CGN1MtD6h3zml-xCDHq4RzEPSq6JpVYL7KGLjk4eHvYfyEbEbmwICZuuodgwbQPQ2DK6aU97IpmaNqXICNZZxbrZF_L5trr2lV-xGpdwxrpCcCoeEE15DAD1gEsDaune80p3uQdUvxmCuX9hpQ4SSjodATLJDAgsKeguU-TcRC7lWnjbm1t692owkzf-PIvVhmqeQ"
         }
      },
      {
         "@context":[
            "https://www.w3.org/2018/credentials/v1"
         ],
         "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/077849c113d60b532c273d132c0443e0c8604a71157ae1348e42e68153971e5e/data.json",
         "type":[
            "VerifiableCredential",
            "SelfAssessedComplianceCriteriaCredential"
         ],
         "issuer":"did:web:dufourstorage.provider.gaia-x.community",
         "expirationDate":"2023-06-21T15:31:47.87+02:00",
         "credentialSubject":{
            "grantsComplianceCriteriaCombination":[
               "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json",
               "did:web:gaia-x.community/compliance-criterion/ea307320071d2c97ba1fc264ac2b57fd571a30fd4bc08b85139ca3256a726b97/data.json",
               "did:web:gaia-x.community/compliance-criterion/2ee23dc8f147f6e8e5707f92161b2695d25c2571af06f832d55eb3dcd01109ca/data.json",
               "did:web:gaia-x.community/compliance-criterion/362213edf62b155d61e6d72c0dde5d7a01b982218eb5a2b00e7b1bb6015dee98/data.json",
               "did:web:gaia-x.community/compliance-criterion/1f2872e3642e6d616bf6af0a3f4c14ef6ca6111e6854184cfbf3cb4ef88a15d5/data.json",
               "did:web:gaia-x.community/compliance-criterion/1df51e3055940bad2b48205d85db6fc7968af811ce74a9383a99dd9bef548340/data.json",
               "did:web:gaia-x.community/compliance-criterion/c754817b22a03a60efc60569ef8cf483fbf0df402d1f72c863967ab393e9b3af/data.json",
               "did:web:gaia-x.community/compliance-criterion/10e1999e6107d991f3251a57252ca95b7564d7817f34ffcdb2520a1939746749/data.json",
               "did:web:gaia-x.community/compliance-criterion/3cb671f9bb51069b27ff31208025553f6386e7d37a7429dd058a9b5924fe64fc/data.json",
               "did:web:gaia-x.community/compliance-criterion/958b517ec144386f06c238dfafa801a4e719807da0a261ecdb1546cd1246a16c/data.json",
               "did:web:gaia-x.community/compliance-criterion/5c6c37938ccec54ddbe569d1e429658d7c3a86b3cb91f5d7b9fa81b61efe6eba/data.json",
               "did:web:gaia-x.community/compliance-criterion/9975196b786b14f3992114e5e1f1cd35befdb5e7fa439e2f94bef927f328ac49/data.json",
               "did:web:gaia-x.community/compliance-criterion/18e49d1e6c56efb00f43a863c87d147e8598a450a683ab6e0db245b7d4d2af86/data.json",
               "did:web:gaia-x.community/compliance-criterion/471b984f28f8a79f7c11f3e44fe298aab7152efc5f97d29fea6d45f2e9a0efd1/data.json",
               "did:web:gaia-x.community/compliance-criterion/98d0eec88ce471c8a0485636b95b7b5f28a743131e84c79a937e8c92de7e330c/data.json",
               "did:web:gaia-x.community/compliance-criterion/c51a27275cbb87b247d21b81ef16f585d114c22e48913eb6d9fb4f0e3aa01ba6/data.json",
               "did:web:gaia-x.community/compliance-criterion/69e118feec07463d4e827aa345ee659ce32e5075e6ddcf8daff7027a99f8893c/data.json",
               "did:web:gaia-x.community/compliance-criterion/25d78bede67bb195d69b9419808d74015c3d5f4a2d733c676a7244b3950ce1b9/data.json",
               "did:web:gaia-x.community/compliance-criterion/843d0a4650499b09e37b5dd7759da4ddbc4f7d7098bc806149a8829f9ea0f315/data.json",
               "did:web:gaia-x.community/compliance-criterion/36c42f80bc75fb96d66f64f1ae4f75798c7906f2a515adfae982367d3ace56dd/data.json",
               "did:web:gaia-x.community/compliance-criterion/c9280867077e758137a1b6ce8010698e187a98e1ebf01fc28e64be76611528ad/data.json",
               "did:web:gaia-x.community/compliance-criterion/53ff2d76126a8b7404fc769734043b3a24d5e7422029cb2666f2b8a4a11fb49b/data.json",
               "did:web:gaia-x.community/compliance-criterion/12f11e3c592b4285d3f051f933a99517b997a543d5705d8717a40af6bc150b2a/data.json",
               "did:web:gaia-x.community/compliance-criterion/e06d36d7b023d7b1da6ef0c9f8d451b8fb1ddecd4538e03d9c63fa3d1c59d990/data.json",
               "did:web:gaia-x.community/compliance-criterion/74b947356bc21c2daaf7f0984b53a1493ed4678f53680d63830842bd22fe352f/data.json",
               "did:web:gaia-x.community/compliance-criterion/f36bebe520dbf1736c3840769828d093791053c5aa6c6a46d1848595e00140bc/data.json",
               "did:web:gaia-x.community/compliance-criterion/9e45260877e1c896508618cefd9f16c185e72dd89fba426eb3901eff167d2360/data.json",
               "did:web:gaia-x.community/compliance-criterion/d604a332d9ca9c8f2b93fc779b35ec2e57818fdedba5d8cef260e69af524b8f2/data.json",
               "did:web:gaia-x.community/compliance-criterion/db57c2316ae02bfb8c4b66368735234fb917995b91123775b164cbbbfa19ca6f/data.json",
               "did:web:gaia-x.community/compliance-criterion/7bedfaaa1ed74be7de48eafac21cfbad4856c424f1638d1af0a2053d2cb227a9/data.json",
               "did:web:gaia-x.community/compliance-criterion/85044fd562ef04649b9d5530e7f4ef248ae6ab81b3c9fa1f4fcd3847ce1c94f1/data.json",
               "did:web:gaia-x.community/compliance-criterion/fb797087b3951edc3add77837cd5401cacc7f2fc206413ff3c9cbff563dd8f68/data.json",
               "did:web:gaia-x.community/compliance-criterion/cde1519cd7f68cce2951987c72d75decf3626a403e89d458fbceb7ac46f06f5a/data.json",
               "did:web:gaia-x.community/compliance-criterion/c4a921a3a3df7fa678c0a521a22a5dac0528bed85233719f71c7cbf52edf302a/data.json",
               "did:web:gaia-x.community/compliance-criterion/6dcd080420ba9a1f7db82682395ed54aacdda5b6ba7c001eec61e2d19226dd2a/data.json",
               "did:web:gaia-x.community/compliance-criterion/a1f306c65a2969c1233cb0142034c0ab21dcac4615f21ad45cba9a8e4e273653/data.json",
               "did:web:gaia-x.community/compliance-criterion/273a31001e5ea054732d0983c44343612a339b6c5e9c5eb2b1209d77487ad94e/data.json",
               "did:web:gaia-x.community/compliance-criterion/1d0535639c6c2e26790ac19a790ad547ac1b4d7c12bdd0f8dd4c3f8c726fc7e5/data.json",
               "did:web:gaia-x.community/compliance-criterion/f5284af5d5bf3b1a80a90c65f65f3ff15fa15ca524085685bae85ca135d62782/data.json",
               "did:web:gaia-x.community/compliance-criterion/733aca07e2d2e11cba61dc74e387d6ca289ee895e521c9c091141be15118c5ba/data.json",
               "did:web:gaia-x.community/compliance-criterion/75571bb4c63239f0df7168e1a44ebdf278fd37738eff607cfb9049c7c2bd083b/data.json",
               "did:web:gaia-x.community/compliance-criterion/cf15736ee674b6fbe9f32bc91a9a81a2b1b1135517a352037edfb0808cddba81/data.json",
               "did:web:gaia-x.community/compliance-criterion/f223175e85cf168bd7df2858c32b5db5267772986299221f4bba5762bd8482c8/data.json",
               "did:web:gaia-x.community/compliance-criterion/3c6be9b458b4402c708733302056fd55aa58d6f1e865b61b3bf03f3db6145e47/data.json",
               "did:web:gaia-x.community/compliance-criterion/b388093c6117171072423b78a1d223caeb66d79266301e695df169d1ab391c79/data.json",
               "did:web:gaia-x.community/compliance-criterion/d6c80de92f9426e57b637f56000530e5142a085abd704e63d708ad9eb68db5dc/data.json",
               "did:web:gaia-x.community/compliance-criterion/ed6a2f7f46f6faacf8142fda2733383ae6b268ccbdadf3293fa4eeb35daf8091/data.json",
               "did:web:gaia-x.community/compliance-criterion/33ffebcc9d1c490ddee3c16f039f801a035a8fe18f88262a7fd2d6b517b4c39e/data.json",
               "did:web:gaia-x.community/compliance-criterion/26c3d521a326ee292fd20ce72f3dfffe0ffd290769283f2a2a7738e8a6847d40/data.json",
               "did:web:gaia-x.community/compliance-criterion/bcc7f7d1ca3d08cc612e3fb3aacc4e3e9e57fd4f15fd6610fc1027f3cfd7ab10/data.json",
               "did:web:gaia-x.community/compliance-criterion/dfb922e542fe48da698dfd1599053f203311ca650003a786e88cc392ae54b698/data.json",
               "did:web:gaia-x.community/compliance-criterion/2808237322eab75f3d041469954dc61bcaab3153b63558d7f8f4449c4bfcc894/data.json",
               "did:web:gaia-x.community/compliance-criterion/bc3a08b25f3e84f6e0bbc06e03b038cc04c9d3eee2a654d956e98f6360e6baa0/data.json"
            ]
         },
         "issuanceDate":"2023-03-23T13:37:05.126757+00:00",
         "proof":{
            "type":"JsonWebSignature2020",
            "proofPurpose":"assertionMethod",
            "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
            "created":"2023-03-23T13:37:05.126757+00:00",
            "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..K_IPBOEMUiClEjdCzaR0F83C_pSGRR24fNIT69-ZlBbuyzhutZiFTRBwd7zBUmICi_d8xMf3wp8aQQcypfekCU1uUXoGtlrxuc3cvhh4tO38_95zCiOEdyJ8_ZMv2QfyyxCv6OCH-AtSzkKVQ0Gdx_r7myVnN6mP5oftyDId0QJM6_GjRdyQxUM5UXZofHZ9b-OAyGfiM9R1ayBf7r2lwYNZNd8vmN67O_AFMnEkTgG9DhG42TwdVvpgyD002TaXrT8fo05ISx5cSGP5KVJ0yqe6eDx0k8e3OIlyrIC8R-wy6C9NJOkn_g1LJ0-vCOfJvc9rHvAg74QDkHKcW_yKkw"
         }
      }
   ],
   "proof":{
      "type":"JsonWebSignature2020",
      "proofPurpose":"assertionMethod",
      "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
      "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..BtcnneOWDOgKVVywlBBd4RnDh3gSn17lw53bUt4wHPVLELdkjvVj2SPSVXbkCOxuTKuO1O2nimTTkMBR5skCM_gKQscr1TX24jbPgEdrdOLa0o1_AsiilGNgDlDQvAXQI7MRCQPrHsTai5Ur8wOfeGwJDopD9t4FDMmz-wtf5RK8vJRIlaUkXqX1aPovdmdHcNsAnhFgRHt6mf0qMZfDbv0rHiTqf4G5sMtvrPeGy55HAQvGje3-A38nnGN4F46MjW9sxKXuXTJF9JML6WHCnlwRAUzgCgJ23MAsO4gEYAaQ3W-fd4RnJNUV2BWMqxV-EUbOKMy5TbUTR1tsobXt2g"
   }
}
```

### s2 - Participant Dufour Storage - Service Offering Storage - Compliance Certification Credential
```json 
{
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "type": [
                "VerifiableCredential",
                "ServiceOfferingCredentialExperimental"
            ],
            "id": "https://catalogue.gaia-x.eu/credentials/ServiceOfferingCredentialExperimental/1679579636460",
            "issuer": "did:web:compliance.abc-federation.gaia-x.community",
            "issuanceDate": "2023-03-23T13:53:56.460Z",
            "expirationDate": "2023-06-21T13:53:56.460Z",
            "credentialSubject": {
                "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
                "hash": "3e30bb6a868f7efa53498024092027d1799c1b435b007afbf4ab9a0b4646a595"
            },
            "proof": {
                "type": "JsonWebSignature2020",
                "created": "2023-03-23T13:53:56.460Z",
                "proofPurpose": "assertionMethod",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..Br8F-jZ6p55gQwzzi_r-o-rZEcuDCxE8q2mggHkAznLuoF7Tay_ySONKhVnwfLyPEPkzOxT1EAyiXTGSYsoYQ1ertWNhIFrjECuUofuoElySAYjYJ1C7rcZCKfhT9jx4npNEcJTNEfiGk_G9rdpG9qIqIzSndHAeIpdFs2SmNObLnPUzY-FMQ5aTwNZGjtveFuojln7mER4blkviV-StHidoGLwnzRKiARVnX2_mKQo2CZ0-NLHl6tAqKKvrgRZ0CugbRIP2pxQIkG08Uazj67ZdR-wcblGI89lXWBpBv1alFGYTUn6uhXyltdHt5P3-00XcPc8SGdB31s8tINOtlw",
                "verificationMethod": "did:web:compliance.abc-federation.gaia-x.community"
            }
        }
```


```json

{
    "@context": [
      "https://www.w3.org/2018/credentials/v1"
    ],
    "type": [
      "VerifiableCredential",
      "ServiceOfferingCredentialExperimental"
    ],
    "id": "https://catalogue.gaia-x.eu/credentials/ServiceOfferingCredentialExperimental/1679479938464",
    "issuer": "did:web:compliance.abc-federation.gaia-x.community",
    "issuanceDate": "2023-03-22T10:12:18.464Z",
    "expirationDate": "2023-06-20T10:12:18.464Z",
    "credentialSubject": {
      "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/3a748292f6a45fec549803534f85b55e426fbbf85e14f8954fdd7d04d789d22f/data.json",
      "hash": "42872360f38855eb0e7926127c3f6b94f4a5f2267e34fb6f8e97db630233fd95"
    },
    "proof": {
      "type": "JsonWebSignature2020",
      "created": "2023-03-22T10:12:18.464Z",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..WVOvA5UUMAH2rVzORwp2gfkrioAhdE_mCjAz0yU1dY2rRpYpzehw7RaaehkHm5dKRUZx21cvqb61GRLGK9m63qHMMMs485t5b5xFme3U_GDrKUqjlUdAX0HVQx9LFA3S8_jHQjQiVrH9y-GSO1qSQk9MKDVk17u5PzGAdfR59-8NMGOLMYfxmBZO3qRQ2hx6bT2kiDH6Y4ZCi08Xfl9BdsKD3G1HOdwSxjsCB1HyQsa_WwhKqqxAJYM0XYeuGG3Dvp8IMdCVXVsdvERSeq57c1WxllLDE6jBPg_Xu8loAj7OctZrPvYi4ZWk6r_aMf7D6hIeBQAmblqMKeVimubo9w",
      "verificationMethod": "did:web:compliance.abc-federation.gaia-x.community"
    }
  }

  ```

### s2 - Participant Dufour Storage - Service Offering Storage - Service Offering Self-Description (end of S1)
```json
{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "@id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vp/1cf8ad940dcc7215b34a82c37873cf864ad5b00173c1c275e1354a637660d965/data.json",
    "@type": [
        "verifiablePresentation"
    ],
    "expirationDate": "2023-06-21T15:31:47.87+02:00",
    "verifiableCredential": [
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "@id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
            "type": [
                "VerifiableCredential",
                "ServiceOfferingExperimental"
            ],
            "issuer": "did:web:dufourstorage.provider.gaia-x.community",
            "expirationDate": "2023-06-21T15:31:47.87+02:00",
            "credentialSubject": {
                "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
                "gx-service-offering:providedBy": "https://docaposte.provider.gaia-x.community/participant/44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5be831bb07d4f260f/compliance-certificate-claim/vc/data.json",
                "gx-terms-and-conditions:serviceTermsAndConditions": {
                    "gx-terms-and-conditions:value": "https://compliance.lab.gaia-x.eu/development",
                    "gx-terms-and-conditions:hash": "myrandomhash"
                },
                "gx-service-offering:title": "DufourServiceTest",
                "gx-service-offering:serviceType": [
                    "Authentication",
                    "Storage"
                ],
                "gx-service-offering:description": "Teste",
                "gx-service-offering:descriptionMarkDown": "",
                "gx-service-offering:webAddress": "",
                "gx-service-offering:dataProtectionRegime": "",
                "gx-service-offering:dataExport": [
                    {
                        "gx-service-offering:requestType": "email",
                        "gx-service-offering:accessType": "digital",
                        "gx-service-offering:formatType": "mime/png"
                    }
                ],
                "gx-service-offering:dependsOn": "",
                "gx-service-offering:keyword": "",
                "gx-service-offering:layer": "IAAS",
                "gx-service-offering:isAvailableOn": [
                    "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
                ]
            },
            "issuanceDate": "2023-03-23T13:51:15.713775+00:00",
            "proof": {
                "type": "JsonWebSignature2020",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
                "created": "2023-03-23T13:51:15.713775+00:00",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..S_EUSotspBCjzY1IS3BCyAilADTxyft-l0FIsWkO99Ey_8bVYUOd76AARaY7Ru5fj7COb3_pc9SDYEoWyTmt6nOakPLWVxxQRZn-8wh-J2kbJDr4kaaFUYodXqk8NI4-5-ZiSGrPG_hx00BI4CGN1MtD6h3zml-xCDHq4RzEPSq6JpVYL7KGLjk4eHvYfyEbEbmwICZuuodgwbQPQ2DK6aU97IpmaNqXICNZZxbrZF_L5trr2lV-xGpdwxrpCcCoeEE15DAD1gEsDaune80p3uQdUvxmCuX9hpQ4SSjodATLJDAgsKeguU-TcRC7lWnjbm1t692owkzf-PIvVhmqeQ"
            }
        },
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "@id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/077849c113d60b532c273d132c0443e0c8604a71157ae1348e42e68153971e5e/data.json",
            "type": [
                "VerifiableCredential",
                "SelfAssessedComplianceCriteriaCredential"
            ],
            "issuer": "did:web:dufourstorage.provider.gaia-x.community",
            "expirationDate": "2023-06-21T15:31:47.87+02:00",
            "credentialSubject": {
                "grantsComplianceCriteriaCombination": [
                    "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json",
                    "did:web:gaia-x.community/compliance-criterion/ea307320071d2c97ba1fc264ac2b57fd571a30fd4bc08b85139ca3256a726b97/data.json",
                    "did:web:gaia-x.community/compliance-criterion/2ee23dc8f147f6e8e5707f92161b2695d25c2571af06f832d55eb3dcd01109ca/data.json",
                    "did:web:gaia-x.community/compliance-criterion/362213edf62b155d61e6d72c0dde5d7a01b982218eb5a2b00e7b1bb6015dee98/data.json",
                    "did:web:gaia-x.community/compliance-criterion/1f2872e3642e6d616bf6af0a3f4c14ef6ca6111e6854184cfbf3cb4ef88a15d5/data.json",
                    "did:web:gaia-x.community/compliance-criterion/1df51e3055940bad2b48205d85db6fc7968af811ce74a9383a99dd9bef548340/data.json",
                    "did:web:gaia-x.community/compliance-criterion/c754817b22a03a60efc60569ef8cf483fbf0df402d1f72c863967ab393e9b3af/data.json",
                    "did:web:gaia-x.community/compliance-criterion/10e1999e6107d991f3251a57252ca95b7564d7817f34ffcdb2520a1939746749/data.json",
                    "did:web:gaia-x.community/compliance-criterion/3cb671f9bb51069b27ff31208025553f6386e7d37a7429dd058a9b5924fe64fc/data.json",
                    "did:web:gaia-x.community/compliance-criterion/958b517ec144386f06c238dfafa801a4e719807da0a261ecdb1546cd1246a16c/data.json",
                    "did:web:gaia-x.community/compliance-criterion/5c6c37938ccec54ddbe569d1e429658d7c3a86b3cb91f5d7b9fa81b61efe6eba/data.json",
                    "did:web:gaia-x.community/compliance-criterion/9975196b786b14f3992114e5e1f1cd35befdb5e7fa439e2f94bef927f328ac49/data.json",
                    "did:web:gaia-x.community/compliance-criterion/18e49d1e6c56efb00f43a863c87d147e8598a450a683ab6e0db245b7d4d2af86/data.json",
                    "did:web:gaia-x.community/compliance-criterion/471b984f28f8a79f7c11f3e44fe298aab7152efc5f97d29fea6d45f2e9a0efd1/data.json",
                    "did:web:gaia-x.community/compliance-criterion/98d0eec88ce471c8a0485636b95b7b5f28a743131e84c79a937e8c92de7e330c/data.json",
                    "did:web:gaia-x.community/compliance-criterion/c51a27275cbb87b247d21b81ef16f585d114c22e48913eb6d9fb4f0e3aa01ba6/data.json",
                    "did:web:gaia-x.community/compliance-criterion/69e118feec07463d4e827aa345ee659ce32e5075e6ddcf8daff7027a99f8893c/data.json",
                    "did:web:gaia-x.community/compliance-criterion/25d78bede67bb195d69b9419808d74015c3d5f4a2d733c676a7244b3950ce1b9/data.json",
                    "did:web:gaia-x.community/compliance-criterion/843d0a4650499b09e37b5dd7759da4ddbc4f7d7098bc806149a8829f9ea0f315/data.json",
                    "did:web:gaia-x.community/compliance-criterion/36c42f80bc75fb96d66f64f1ae4f75798c7906f2a515adfae982367d3ace56dd/data.json",
                    "did:web:gaia-x.community/compliance-criterion/c9280867077e758137a1b6ce8010698e187a98e1ebf01fc28e64be76611528ad/data.json",
                    "did:web:gaia-x.community/compliance-criterion/53ff2d76126a8b7404fc769734043b3a24d5e7422029cb2666f2b8a4a11fb49b/data.json",
                    "did:web:gaia-x.community/compliance-criterion/12f11e3c592b4285d3f051f933a99517b997a543d5705d8717a40af6bc150b2a/data.json",
                    "did:web:gaia-x.community/compliance-criterion/e06d36d7b023d7b1da6ef0c9f8d451b8fb1ddecd4538e03d9c63fa3d1c59d990/data.json",
                    "did:web:gaia-x.community/compliance-criterion/74b947356bc21c2daaf7f0984b53a1493ed4678f53680d63830842bd22fe352f/data.json",
                    "did:web:gaia-x.community/compliance-criterion/f36bebe520dbf1736c3840769828d093791053c5aa6c6a46d1848595e00140bc/data.json",
                    "did:web:gaia-x.community/compliance-criterion/9e45260877e1c896508618cefd9f16c185e72dd89fba426eb3901eff167d2360/data.json",
                    "did:web:gaia-x.community/compliance-criterion/d604a332d9ca9c8f2b93fc779b35ec2e57818fdedba5d8cef260e69af524b8f2/data.json",
                    "did:web:gaia-x.community/compliance-criterion/db57c2316ae02bfb8c4b66368735234fb917995b91123775b164cbbbfa19ca6f/data.json",
                    "did:web:gaia-x.community/compliance-criterion/7bedfaaa1ed74be7de48eafac21cfbad4856c424f1638d1af0a2053d2cb227a9/data.json",
                    "did:web:gaia-x.community/compliance-criterion/85044fd562ef04649b9d5530e7f4ef248ae6ab81b3c9fa1f4fcd3847ce1c94f1/data.json",
                    "did:web:gaia-x.community/compliance-criterion/fb797087b3951edc3add77837cd5401cacc7f2fc206413ff3c9cbff563dd8f68/data.json",
                    "did:web:gaia-x.community/compliance-criterion/cde1519cd7f68cce2951987c72d75decf3626a403e89d458fbceb7ac46f06f5a/data.json",
                    "did:web:gaia-x.community/compliance-criterion/c4a921a3a3df7fa678c0a521a22a5dac0528bed85233719f71c7cbf52edf302a/data.json",
                    "did:web:gaia-x.community/compliance-criterion/6dcd080420ba9a1f7db82682395ed54aacdda5b6ba7c001eec61e2d19226dd2a/data.json",
                    "did:web:gaia-x.community/compliance-criterion/a1f306c65a2969c1233cb0142034c0ab21dcac4615f21ad45cba9a8e4e273653/data.json",
                    "did:web:gaia-x.community/compliance-criterion/273a31001e5ea054732d0983c44343612a339b6c5e9c5eb2b1209d77487ad94e/data.json",
                    "did:web:gaia-x.community/compliance-criterion/1d0535639c6c2e26790ac19a790ad547ac1b4d7c12bdd0f8dd4c3f8c726fc7e5/data.json",
                    "did:web:gaia-x.community/compliance-criterion/f5284af5d5bf3b1a80a90c65f65f3ff15fa15ca524085685bae85ca135d62782/data.json",
                    "did:web:gaia-x.community/compliance-criterion/733aca07e2d2e11cba61dc74e387d6ca289ee895e521c9c091141be15118c5ba/data.json",
                    "did:web:gaia-x.community/compliance-criterion/75571bb4c63239f0df7168e1a44ebdf278fd37738eff607cfb9049c7c2bd083b/data.json",
                    "did:web:gaia-x.community/compliance-criterion/cf15736ee674b6fbe9f32bc91a9a81a2b1b1135517a352037edfb0808cddba81/data.json",
                    "did:web:gaia-x.community/compliance-criterion/f223175e85cf168bd7df2858c32b5db5267772986299221f4bba5762bd8482c8/data.json",
                    "did:web:gaia-x.community/compliance-criterion/3c6be9b458b4402c708733302056fd55aa58d6f1e865b61b3bf03f3db6145e47/data.json",
                    "did:web:gaia-x.community/compliance-criterion/b388093c6117171072423b78a1d223caeb66d79266301e695df169d1ab391c79/data.json",
                    "did:web:gaia-x.community/compliance-criterion/d6c80de92f9426e57b637f56000530e5142a085abd704e63d708ad9eb68db5dc/data.json",
                    "did:web:gaia-x.community/compliance-criterion/ed6a2f7f46f6faacf8142fda2733383ae6b268ccbdadf3293fa4eeb35daf8091/data.json",
                    "did:web:gaia-x.community/compliance-criterion/33ffebcc9d1c490ddee3c16f039f801a035a8fe18f88262a7fd2d6b517b4c39e/data.json",
                    "did:web:gaia-x.community/compliance-criterion/26c3d521a326ee292fd20ce72f3dfffe0ffd290769283f2a2a7738e8a6847d40/data.json",
                    "did:web:gaia-x.community/compliance-criterion/bcc7f7d1ca3d08cc612e3fb3aacc4e3e9e57fd4f15fd6610fc1027f3cfd7ab10/data.json",
                    "did:web:gaia-x.community/compliance-criterion/dfb922e542fe48da698dfd1599053f203311ca650003a786e88cc392ae54b698/data.json",
                    "did:web:gaia-x.community/compliance-criterion/2808237322eab75f3d041469954dc61bcaab3153b63558d7f8f4449c4bfcc894/data.json",
                    "did:web:gaia-x.community/compliance-criterion/bc3a08b25f3e84f6e0bbc06e03b038cc04c9d3eee2a654d956e98f6360e6baa0/data.json"
                ]
            },
            "issuanceDate": "2023-03-23T13:37:05.126757+00:00",
            "proof": {
                "type": "JsonWebSignature2020",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
                "created": "2023-03-23T13:37:05.126757+00:00",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..K_IPBOEMUiClEjdCzaR0F83C_pSGRR24fNIT69-ZlBbuyzhutZiFTRBwd7zBUmICi_d8xMf3wp8aQQcypfekCU1uUXoGtlrxuc3cvhh4tO38_95zCiOEdyJ8_ZMv2QfyyxCv6OCH-AtSzkKVQ0Gdx_r7myVnN6mP5oftyDId0QJM6_GjRdyQxUM5UXZofHZ9b-OAyGfiM9R1ayBf7r2lwYNZNd8vmN67O_AFMnEkTgG9DhG42TwdVvpgyD002TaXrT8fo05ISx5cSGP5KVJ0yqe6eDx0k8e3OIlyrIC8R-wy6C9NJOkn_g1LJ0-vCOfJvc9rHvAg74QDkHKcW_yKkw"
            }
        },
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "type": [
                "VerifiableCredential",
                "ServiceOfferingCredentialExperimental"
            ],
            "id": "https://catalogue.gaia-x.eu/credentials/ServiceOfferingCredentialExperimental/1679579636460",
            "issuer": "did:web:compliance.abc-federation.gaia-x.community",
            "issuanceDate": "2023-03-23T13:53:56.460Z",
            "expirationDate": "2023-06-21T13:53:56.460Z",
            "credentialSubject": {
                "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
                "hash": "3e30bb6a868f7efa53498024092027d1799c1b435b007afbf4ab9a0b4646a595"
            },
            "proof": {
                "type": "JsonWebSignature2020",
                "created": "2023-03-23T13:53:56.460Z",
                "proofPurpose": "assertionMethod",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..Br8F-jZ6p55gQwzzi_r-o-rZEcuDCxE8q2mggHkAznLuoF7Tay_ySONKhVnwfLyPEPkzOxT1EAyiXTGSYsoYQ1ertWNhIFrjECuUofuoElySAYjYJ1C7rcZCKfhT9jx4npNEcJTNEfiGk_G9rdpG9qIqIzSndHAeIpdFs2SmNObLnPUzY-FMQ5aTwNZGjtveFuojln7mER4blkviV-StHidoGLwnzRKiARVnX2_mKQo2CZ0-NLHl6tAqKKvrgRZ0CugbRIP2pxQIkG08Uazj67ZdR-wcblGI89lXWBpBv1alFGYTUn6uhXyltdHt5P3-00XcPc8SGdB31s8tINOtlw",
                "verificationMethod": "did:web:compliance.abc-federation.gaia-x.community"
            }
        }
    ],
    "proof": {
        "type": "JsonWebSignature2020",
        "proofPurpose": "assertionMethod",
        "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
        "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..BtcnneOWDOgKVVywlBBd4RnDh3gSn17lw53bUt4wHPVLELdkjvVj2SPSVXbkCOxuTKuO1O2nimTTkMBR5skCM_gKQscr1TX24jbPgEdrdOLa0o1_AsiilGNgDlDQvAXQI7MRCQPrHsTai5Ur8wOfeGwJDopD9t4FDMmz-wtf5RK8vJRIlaUkXqX1aPovdmdHcNsAnhFgRHt6mf0qMZfDbv0rHiTqf4G5sMtvrPeGy55HAQvGje3-A38nnGN4F46MjW9sxKXuXTJF9JML6WHCnlwRAUzgCgJ23MAsO4gEYAaQ3W-fd4RnJNUV2BWMqxV-EUbOKMy5TbUTR1tsobXt2g"
    }
}
```

{
    "@context": [
        "https://www.w3.org/2018/credentials/v1"
    ],
    "@id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vp/5920a2482f792880d5b5490d0816175cdc6145349f2dda5eebc63fe8d1561259/data.json",
    "@type": [
        "verifiablePresentation"
    ],
    "expirationDate": "2023-06-20T11:55:33.688+02:00",
    "verifiableCredential": [
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "@id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/3a748292f6a45fec549803534f85b55e426fbbf85e14f8954fdd7d04d789d22f/data.json",
            "type": [
                "VerifiableCredential",
                "ServiceOfferingExperimental"
            ],
            "issuer": "did:web:dufourstorage.provider.gaia-x.community",
            "expirationDate": "2023-06-20T11:55:33.688+02:00",
            "credentialSubject": {
                "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/3a748292f6a45fec549803534f85b55e426fbbf85e14f8954fdd7d04d789d22f/data.json",
                "gx-service-offering:providedBy": "https://dufourstorage.provider.dev.gaiax.ovh/participant/1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/ce179ab2440f4b58bad8ae49666e09a0/data.json",
                "gx-terms-and-conditions:serviceTermsAndConditions": {
                    "gx-terms-and-conditions:value": "https://compliance.lab.gaia-x.eu/development",
                    "gx-terms-and-conditions:hash": "myrandomhash"
                },
                "gx-service-offering:title": "DufourServiceH",
                "gx-service-offering:serviceType": [
                    "Networking"
                ],
                "gx-service-offering:description": "Description",
                "gx-service-offering:descriptionMarkDown": "",
                "gx-service-offering:webAddress": "",
                "gx-service-offering:dataProtectionRegime": "",
                "gx-service-offering:dataExport": [
                    {
                        "gx-service-offering:requestType": "email",
                        "gx-service-offering:accessType": "digital",
                        "gx-service-offering:formatType": "mime/png"
                    }
                ],
                "gx-service-offering:dependsOn": "",
                "gx-service-offering:keyword": "",
                "gx-service-offering:layer": "PAAS",
                "gx-service-offering:isAvailableOn": [
                    "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
                ]
            },
            "issuanceDate": "2023-03-22T09:57:10.330600+00:00",
            "proof": {
                "type": "JsonWebSignature2020",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
                "created": "2023-03-22T09:57:10.330600+00:00",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..fgB-VcQ8y23HJBiYLb98ZBu8OLpqZyqW57UuFmFBtLXeZ4F5pWPXiAehsjiOkiDg4yLNcbEd6ry-zvKTIHaRGw1VK22qSNh48FF5U7tu-N72uXDN1cIIEqK_9XsbaLRn1TcV-aTdcCQmu7t8oUWKXtslsmCP9BXNtNtur4oLA0edLM8rulCV6-649J0Z02qGWbQsBXWuV17saIYKyBPtQyMc8ZC7yJ10VhvoUw3-hiKYybk2XxSnqQZ0YWzXyyZG6LnQ7tkIQIEfFKupg-DVB1P-_cLjNoH5QT5O77NaoCnSwU3HrQZbMMsKsaIu2nXJPTR1lqY9rPya8jvhqOYipw"
            }
        },
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "@id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/05717acbedb041656563eb037a94e6e84d530b4de700668858e61b74baa10b7e/data.json",
            "type": [
                "VerifiableCredential",
                "SelfAssessedComplianceCriteriaCredential"
            ],
            "issuer": "did:web:dufourstorage.provider.gaia-x.community",
            "expirationDate": "2023-06-20T11:55:33.688+02:00",
            "credentialSubject": {
                "grantsComplianceCriteriaCombination": [
                    "did:web:gaia-x.community/compliance-criterion/b272ac3eba05f1b37802818a46eff6b6c5edb34df2dfa54430519752d5aafa63/data.json",
                    "did:web:gaia-x.community/compliance-criterion/99127352038197c80d2a2dd8a79b3ea576a71904b4e14a2a1cbee311dade8f72/data.json",
                    "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json",
                    "did:web:gaia-x.community/compliance-criterion/ea307320071d2c97ba1fc264ac2b57fd571a30fd4bc08b85139ca3256a726b97/data.json",
                    "did:web:gaia-x.community/compliance-criterion/996d747c5c86cdda911eb524b584e4609262787ca202a2aca9e56b6ebe6fd9ef/data.json",
                    "did:web:gaia-x.community/compliance-criterion/7f8531d43f662c8340090b817e40d1c37f5ea99781fc7ac3dc970937ab6f752a/data.json",
                    "did:web:gaia-x.community/compliance-criterion/2ee23dc8f147f6e8e5707f92161b2695d25c2571af06f832d55eb3dcd01109ca/data.json",
                    "did:web:gaia-x.community/compliance-criterion/24339da30191ca31a0b505570119f50acb3567c4083cc98762c3375575589516/data.json",
                    "did:web:gaia-x.community/compliance-criterion/a5ad1d8a9f50565e6bb1ead2dd35414930aff9b601423625ef88db452ccb1c46/data.json",
                    "did:web:gaia-x.community/compliance-criterion/362213edf62b155d61e6d72c0dde5d7a01b982218eb5a2b00e7b1bb6015dee98/data.json",
                    "did:web:gaia-x.community/compliance-criterion/60fa3eb419b59b4a268f23f3a91edc6531cb6bf27271040c4071b006dc381add/data.json",
                    "did:web:gaia-x.community/compliance-criterion/5ed119518c1238b9e1c02f3710fc70aba5407106a8738be9b731eb009493187d/data.json"
                ]
            },
            "issuanceDate": "2023-03-22T09:57:25.476124+00:00",
            "proof": {
                "type": "JsonWebSignature2020",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
                "created": "2023-03-22T09:57:25.476124+00:00",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..gVOg86rhJqDA380fGRtNWxrcX7yiaXDGXiCBMBYtBKqilddyoN_WgrVeRz8XOJpMVWVwfJ5O7H7cTWYk8Hlopg6M25R8C-cCEEu2F9pTZ-zQKsBhKyDqyYA2PrTnhAbVraG25UfdTaVUzaKD236jyw4OlVrrYae9SD_s2TG7FVQ8wEE1IIySroEmrLc4DuS2kMFV8bwdcGeZfZcg6qJrFt3OhfxUJ1PpNzjivfgBGfuH4tc1VHQr-US-WPksJuobwMAUQrQu_F_Otx1L-GjytMbGz-FLgA3e8Dxhf3qxJLl0dQ9_7L1cBbje1BNq8JeBDCIXNxW5TJbL_7mxk9lp5g"
            }
        },
{
    "@context": [
      "https://www.w3.org/2018/credentials/v1"
    ],
    "type": [
      "VerifiableCredential",
      "ServiceOfferingCredentialExperimental"
    ],
    "id": "https://catalogue.gaia-x.eu/credentials/ServiceOfferingCredentialExperimental/1679479938464",
    "issuer": "did:web:compliance.abc-federation.gaia-x.community",
    "issuanceDate": "2023-03-22T10:12:18.464Z",
    "expirationDate": "2023-06-20T10:12:18.464Z",
    "credentialSubject": {
      "id": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/3a748292f6a45fec549803534f85b55e426fbbf85e14f8954fdd7d04d789d22f/data.json",
      "hash": "42872360f38855eb0e7926127c3f6b94f4a5f2267e34fb6f8e97db630233fd95"
    },
    "proof": {
      "type": "JsonWebSignature2020",
      "created": "2023-03-22T10:12:18.464Z",
      "proofPurpose": "assertionMethod",
      "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..WVOvA5UUMAH2rVzORwp2gfkrioAhdE_mCjAz0yU1dY2rRpYpzehw7RaaehkHm5dKRUZx21cvqb61GRLGK9m63qHMMMs485t5b5xFme3U_GDrKUqjlUdAX0HVQx9LFA3S8_jHQjQiVrH9y-GSO1qSQk9MKDVk17u5PzGAdfR59-8NMGOLMYfxmBZO3qRQ2hx6bT2kiDH6Y4ZCi08Xfl9BdsKD3G1HOdwSxjsCB1HyQsa_WwhKqqxAJYM0XYeuGG3Dvp8IMdCVXVsdvERSeq57c1WxllLDE6jBPg_Xu8loAj7OctZrPvYi4ZWk6r_aMf7D6hIeBQAmblqMKeVimubo9w",
      "verificationMethod": "did:web:compliance.abc-federation.gaia-x.community"
    }
  }
    ]
}

```
## Step03

### s2 - Participant Dufour Storage - Service Offering Storage - Label SelfAssessedComplianceCriteriaCredential Claims for Service Offering
```json
{
         "@context":[
            "https://www.w3.org/2018/credentials/v1"
         ],
         "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/077849c113d60b532c273d132c0443e0c8604a71157ae1348e42e68153971e5e/data.json",
         "type":[
            "VerifiableCredential",
            "SelfAssessedComplianceCriteriaCredential"
         ],
         "issuer":"did:web:dufourstorage.provider.gaia-x.community",
         "expirationDate":"2023-06-21T15:31:47.87+02:00",
         "credentialSubject":{
            "grantsComplianceCriteriaCombination":[
               "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json",
               "did:web:gaia-x.community/compliance-criterion/ea307320071d2c97ba1fc264ac2b57fd571a30fd4bc08b85139ca3256a726b97/data.json",
               "did:web:gaia-x.community/compliance-criterion/2ee23dc8f147f6e8e5707f92161b2695d25c2571af06f832d55eb3dcd01109ca/data.json",
               "did:web:gaia-x.community/compliance-criterion/362213edf62b155d61e6d72c0dde5d7a01b982218eb5a2b00e7b1bb6015dee98/data.json",
               "did:web:gaia-x.community/compliance-criterion/1f2872e3642e6d616bf6af0a3f4c14ef6ca6111e6854184cfbf3cb4ef88a15d5/data.json",
               "did:web:gaia-x.community/compliance-criterion/1df51e3055940bad2b48205d85db6fc7968af811ce74a9383a99dd9bef548340/data.json",
               "did:web:gaia-x.community/compliance-criterion/c754817b22a03a60efc60569ef8cf483fbf0df402d1f72c863967ab393e9b3af/data.json",
               "did:web:gaia-x.community/compliance-criterion/10e1999e6107d991f3251a57252ca95b7564d7817f34ffcdb2520a1939746749/data.json",
               "did:web:gaia-x.community/compliance-criterion/3cb671f9bb51069b27ff31208025553f6386e7d37a7429dd058a9b5924fe64fc/data.json",
               "did:web:gaia-x.community/compliance-criterion/958b517ec144386f06c238dfafa801a4e719807da0a261ecdb1546cd1246a16c/data.json",
               "did:web:gaia-x.community/compliance-criterion/5c6c37938ccec54ddbe569d1e429658d7c3a86b3cb91f5d7b9fa81b61efe6eba/data.json",
               "did:web:gaia-x.community/compliance-criterion/9975196b786b14f3992114e5e1f1cd35befdb5e7fa439e2f94bef927f328ac49/data.json",
               "did:web:gaia-x.community/compliance-criterion/18e49d1e6c56efb00f43a863c87d147e8598a450a683ab6e0db245b7d4d2af86/data.json",
               "did:web:gaia-x.community/compliance-criterion/471b984f28f8a79f7c11f3e44fe298aab7152efc5f97d29fea6d45f2e9a0efd1/data.json",
               "did:web:gaia-x.community/compliance-criterion/98d0eec88ce471c8a0485636b95b7b5f28a743131e84c79a937e8c92de7e330c/data.json",
               "did:web:gaia-x.community/compliance-criterion/c51a27275cbb87b247d21b81ef16f585d114c22e48913eb6d9fb4f0e3aa01ba6/data.json",
               "did:web:gaia-x.community/compliance-criterion/69e118feec07463d4e827aa345ee659ce32e5075e6ddcf8daff7027a99f8893c/data.json",
               "did:web:gaia-x.community/compliance-criterion/25d78bede67bb195d69b9419808d74015c3d5f4a2d733c676a7244b3950ce1b9/data.json",
               "did:web:gaia-x.community/compliance-criterion/843d0a4650499b09e37b5dd7759da4ddbc4f7d7098bc806149a8829f9ea0f315/data.json",
               "did:web:gaia-x.community/compliance-criterion/36c42f80bc75fb96d66f64f1ae4f75798c7906f2a515adfae982367d3ace56dd/data.json",
               "did:web:gaia-x.community/compliance-criterion/c9280867077e758137a1b6ce8010698e187a98e1ebf01fc28e64be76611528ad/data.json",
               "did:web:gaia-x.community/compliance-criterion/53ff2d76126a8b7404fc769734043b3a24d5e7422029cb2666f2b8a4a11fb49b/data.json",
               "did:web:gaia-x.community/compliance-criterion/12f11e3c592b4285d3f051f933a99517b997a543d5705d8717a40af6bc150b2a/data.json",
               "did:web:gaia-x.community/compliance-criterion/e06d36d7b023d7b1da6ef0c9f8d451b8fb1ddecd4538e03d9c63fa3d1c59d990/data.json",
               "did:web:gaia-x.community/compliance-criterion/74b947356bc21c2daaf7f0984b53a1493ed4678f53680d63830842bd22fe352f/data.json",
               "did:web:gaia-x.community/compliance-criterion/f36bebe520dbf1736c3840769828d093791053c5aa6c6a46d1848595e00140bc/data.json",
               "did:web:gaia-x.community/compliance-criterion/9e45260877e1c896508618cefd9f16c185e72dd89fba426eb3901eff167d2360/data.json",
               "did:web:gaia-x.community/compliance-criterion/d604a332d9ca9c8f2b93fc779b35ec2e57818fdedba5d8cef260e69af524b8f2/data.json",
               "did:web:gaia-x.community/compliance-criterion/db57c2316ae02bfb8c4b66368735234fb917995b91123775b164cbbbfa19ca6f/data.json",
               "did:web:gaia-x.community/compliance-criterion/7bedfaaa1ed74be7de48eafac21cfbad4856c424f1638d1af0a2053d2cb227a9/data.json",
               "did:web:gaia-x.community/compliance-criterion/85044fd562ef04649b9d5530e7f4ef248ae6ab81b3c9fa1f4fcd3847ce1c94f1/data.json",
               "did:web:gaia-x.community/compliance-criterion/fb797087b3951edc3add77837cd5401cacc7f2fc206413ff3c9cbff563dd8f68/data.json",
               "did:web:gaia-x.community/compliance-criterion/cde1519cd7f68cce2951987c72d75decf3626a403e89d458fbceb7ac46f06f5a/data.json",
               "did:web:gaia-x.community/compliance-criterion/c4a921a3a3df7fa678c0a521a22a5dac0528bed85233719f71c7cbf52edf302a/data.json",
               "did:web:gaia-x.community/compliance-criterion/6dcd080420ba9a1f7db82682395ed54aacdda5b6ba7c001eec61e2d19226dd2a/data.json",
               "did:web:gaia-x.community/compliance-criterion/a1f306c65a2969c1233cb0142034c0ab21dcac4615f21ad45cba9a8e4e273653/data.json",
               "did:web:gaia-x.community/compliance-criterion/273a31001e5ea054732d0983c44343612a339b6c5e9c5eb2b1209d77487ad94e/data.json",
               "did:web:gaia-x.community/compliance-criterion/1d0535639c6c2e26790ac19a790ad547ac1b4d7c12bdd0f8dd4c3f8c726fc7e5/data.json",
               "did:web:gaia-x.community/compliance-criterion/f5284af5d5bf3b1a80a90c65f65f3ff15fa15ca524085685bae85ca135d62782/data.json",
               "did:web:gaia-x.community/compliance-criterion/733aca07e2d2e11cba61dc74e387d6ca289ee895e521c9c091141be15118c5ba/data.json",
               "did:web:gaia-x.community/compliance-criterion/75571bb4c63239f0df7168e1a44ebdf278fd37738eff607cfb9049c7c2bd083b/data.json",
               "did:web:gaia-x.community/compliance-criterion/cf15736ee674b6fbe9f32bc91a9a81a2b1b1135517a352037edfb0808cddba81/data.json",
               "did:web:gaia-x.community/compliance-criterion/f223175e85cf168bd7df2858c32b5db5267772986299221f4bba5762bd8482c8/data.json",
               "did:web:gaia-x.community/compliance-criterion/3c6be9b458b4402c708733302056fd55aa58d6f1e865b61b3bf03f3db6145e47/data.json",
               "did:web:gaia-x.community/compliance-criterion/b388093c6117171072423b78a1d223caeb66d79266301e695df169d1ab391c79/data.json",
               "did:web:gaia-x.community/compliance-criterion/d6c80de92f9426e57b637f56000530e5142a085abd704e63d708ad9eb68db5dc/data.json",
               "did:web:gaia-x.community/compliance-criterion/ed6a2f7f46f6faacf8142fda2733383ae6b268ccbdadf3293fa4eeb35daf8091/data.json",
               "did:web:gaia-x.community/compliance-criterion/33ffebcc9d1c490ddee3c16f039f801a035a8fe18f88262a7fd2d6b517b4c39e/data.json",
               "did:web:gaia-x.community/compliance-criterion/26c3d521a326ee292fd20ce72f3dfffe0ffd290769283f2a2a7738e8a6847d40/data.json",
               "did:web:gaia-x.community/compliance-criterion/bcc7f7d1ca3d08cc612e3fb3aacc4e3e9e57fd4f15fd6610fc1027f3cfd7ab10/data.json",
               "did:web:gaia-x.community/compliance-criterion/dfb922e542fe48da698dfd1599053f203311ca650003a786e88cc392ae54b698/data.json",
               "did:web:gaia-x.community/compliance-criterion/2808237322eab75f3d041469954dc61bcaab3153b63558d7f8f4449c4bfcc894/data.json",
               "did:web:gaia-x.community/compliance-criterion/bc3a08b25f3e84f6e0bbc06e03b038cc04c9d3eee2a654d956e98f6360e6baa0/data.json"
            ]
         },
         "issuanceDate":"2023-03-23T13:37:05.126757+00:00",
         "proof":{
            "type":"JsonWebSignature2020",
            "proofPurpose":"assertionMethod",
            "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
            "created":"2023-03-23T13:37:05.126757+00:00",
            "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..K_IPBOEMUiClEjdCzaR0F83C_pSGRR24fNIT69-ZlBbuyzhutZiFTRBwd7zBUmICi_d8xMf3wp8aQQcypfekCU1uUXoGtlrxuc3cvhh4tO38_95zCiOEdyJ8_ZMv2QfyyxCv6OCH-AtSzkKVQ0Gdx_r7myVnN6mP5oftyDId0QJM6_GjRdyQxUM5UXZofHZ9b-OAyGfiM9R1ayBf7r2lwYNZNd8vmN67O_AFMnEkTgG9DhG42TwdVvpgyD002TaXrT8fo05ISx5cSGP5KVJ0yqe6eDx0k8e3OIlyrIC8R-wy6C9NJOkn_g1LJ0-vCOfJvc9rHvAg74QDkHKcW_yKkw"
         }
      }
```
### [](#s2-participant-dufour-storage-service-offering-storage-service-offering-self-description-end-of-s3)s2 - Participant Dufour Storage - Service Offering Storage - GrantedLabel 
```json
{
            "@context": [
                "https://www.w3.org/2018/credentials/v1"
            ],
            "id": "https://labelling.abc-federation.gaia-x.community/vc/8a6d70de-c995-40d8-a2af-a7bc98844ef5",
            "type": [
                "VerifiableCredential",
                "GrantedLabel"
            ],
            "credentialSubject": {
                "id": "did:web:ovhcloud.provider.gaia-x.community:participant:3f9227f3-3ad0-447d-a875-25395453c73e/granted-label/8a6d70de-c995-40d8-a2af-a7bc98844ef5/data.json",
                "type": [
                    "GrantedLabel"
                ],
                "vc": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
                "label": {
                    "hasName": "Gaia-X label Level 1",
                    "id": "did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json"
                },
                "proof-vc": {
                    "type": "JsonWebSignature2020",
                    "proofPurpose": "assertionMethod",
                    "verificationMethod": "did:web:dufourstorage.provider.gaia-x.community",
                    "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..BtcnneOWDOgKVVywlBBd4RnDh3gSn17lw53bUt4wHPVLELdkjvVj2SPSVXbkCOxuTKuO1O2nimTTkMBR5skCM_gKQscr1TX24jbPgEdrdOLa0o1_AsiilGNgDlDQvAXQI7MRCQPrHsTai5Ur8wOfeGwJDopD9t4FDMmz-wtf5RK8vJRIlaUkXqX1aPovdmdHcNsAnhFgRHt6mf0qMZfDbv0rHiTqf4G5sMtvrPeGy55HAQvGje3-A38nnGN4F46MjW9sxKXuXTJF9JML6WHCnlwRAUzgCgJ23MAsO4gEYAaQ3W-fd4RnJNUV2BWMqxV-EUbOKMy5TbUTR1tsobXt2g"
                }
            },
            "expirationDate": "2023-06-21T00:00:00.000Z",
            "issuer": "did:web:abc-federation.gaia-x.community",
            "issuanceDate": "2023-03-23T13:56:02.464898+00:00",
            "proof": {
                "type": "JsonWebSignature2020",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:web:abc-federation.gaia-x.community",
                "created": "2023-03-23T13:56:02.464898+00:00",
                "jws": "eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..JAdJ7Rc2o4EUSAzHdgJmta5IC2KfOKrMO1NDDVPRLjJvkpxl7c13RpKaYLJ5RR3EZPI8JCkVEZf2ILgWOOOg4J7EDX-I9ne7R8AQGWcpEo1JRvCFTRm3FQoe6BUVhd6QVq5vQKe03PpVTp80246Kth8TyIrScsGGlj15aqa2XmyqcqaUl7rbjIqE8ve-T8SIsrQlhvEVxFlvBkbQjXME2juXefF3A1mNnPcKDiGRJme4Wwn5E91BSa7k5z33jw0DnA_-Q0i-BIsLNcAwFQCs7-t8W6_RK6urN1AA5gHH4DpTW8Ru3t7gPtHAL4aOuluDNsiEM-LQcfG4XHPiirb76w"
            }
        }
```

### s2 - Participant Dufour Storage - Service Offering Storage - Service Offering Self-Description (end of S3)
```json
{
   "@context":[
      "https://www.w3.org/2018/credentials/v1"
   ],
   "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vp/1cf8ad940dcc7215b34a82c37873cf864ad5b00173c1c275e1354a637660d965/data.json",
   "@type":[
      "verifiablePresentation"
   ],
   "expirationDate":"2023-06-21T15:31:47.87+02:00",
   "verifiableCredential":[
      {
         "@context":[
            "https://www.w3.org/2018/credentials/v1"
         ],
         "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
         "type":[
            "VerifiableCredential",
            "ServiceOfferingExperimental"
         ],
         "issuer":"did:web:dufourstorage.provider.gaia-x.community",
         "expirationDate":"2023-06-21T15:31:47.87+02:00",
         "credentialSubject":{
            "id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
            "gx-service-offering:providedBy":"https://docaposte.provider.gaia-x.community/participant/44abd1d1db9faafcb2f5a5384d491680ae7bd458b4e12dc5be831bb07d4f260f/compliance-certificate-claim/vc/data.json",
            "gx-terms-and-conditions:serviceTermsAndConditions":{
               "gx-terms-and-conditions:value":"https://compliance.lab.gaia-x.eu/development",
               "gx-terms-and-conditions:hash":"myrandomhash"
            },
            "gx-service-offering:title":"DufourServiceTest",
            "gx-service-offering:serviceType":[
               "Authentication",
               "Storage"
            ],
            "gx-service-offering:description":"Teste",
            "gx-service-offering:descriptionMarkDown":"",
            "gx-service-offering:webAddress":"",
            "gx-service-offering:dataProtectionRegime":"",
            "gx-service-offering:dataExport":[
               {
                  "gx-service-offering:requestType":"email",
                  "gx-service-offering:accessType":"digital",
                  "gx-service-offering:formatType":"mime/png"
               }
            ],
            "gx-service-offering:dependsOn":"",
            "gx-service-offering:keyword":"",
            "gx-service-offering:layer":"IAAS",
            "gx-service-offering:isAvailableOn":[
               "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/location/cdaf622d545337367680b2da15f86f58efb3e8cdcae82b23ebcf1dfb20c04bda/data.json"
            ]
         },
         "issuanceDate":"2023-03-23T13:51:15.713775+00:00",
         "proof":{
            "type":"JsonWebSignature2020",
            "proofPurpose":"assertionMethod",
            "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
            "created":"2023-03-23T13:51:15.713775+00:00",
            "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..S_EUSotspBCjzY1IS3BCyAilADTxyft-l0FIsWkO99Ey_8bVYUOd76AARaY7Ru5fj7COb3_pc9SDYEoWyTmt6nOakPLWVxxQRZn-8wh-J2kbJDr4kaaFUYodXqk8NI4-5-ZiSGrPG_hx00BI4CGN1MtD6h3zml-xCDHq4RzEPSq6JpVYL7KGLjk4eHvYfyEbEbmwICZuuodgwbQPQ2DK6aU97IpmaNqXICNZZxbrZF_L5trr2lV-xGpdwxrpCcCoeEE15DAD1gEsDaune80p3uQdUvxmCuX9hpQ4SSjodATLJDAgsKeguU-TcRC7lWnjbm1t692owkzf-PIvVhmqeQ"
         }
      },
      {
         "@context":[
            "https://www.w3.org/2018/credentials/v1"
         ],
         "@id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/077849c113d60b532c273d132c0443e0c8604a71157ae1348e42e68153971e5e/data.json",
         "type":[
            "VerifiableCredential",
            "SelfAssessedComplianceCriteriaCredential"
         ],
         "issuer":"did:web:dufourstorage.provider.gaia-x.community",
         "expirationDate":"2023-06-21T15:31:47.87+02:00",
         "credentialSubject":{
            "grantsComplianceCriteriaCombination":[
               "did:web:gaia-x.community/compliance-criterion/b628c61b61bf5ea54c76d97913f30a7b249cac68df2ccc428593838cc4ddebe8/data.json",
               "did:web:gaia-x.community/compliance-criterion/ea307320071d2c97ba1fc264ac2b57fd571a30fd4bc08b85139ca3256a726b97/data.json",
               "did:web:gaia-x.community/compliance-criterion/2ee23dc8f147f6e8e5707f92161b2695d25c2571af06f832d55eb3dcd01109ca/data.json",
               "did:web:gaia-x.community/compliance-criterion/362213edf62b155d61e6d72c0dde5d7a01b982218eb5a2b00e7b1bb6015dee98/data.json",
               "did:web:gaia-x.community/compliance-criterion/1f2872e3642e6d616bf6af0a3f4c14ef6ca6111e6854184cfbf3cb4ef88a15d5/data.json",
               "did:web:gaia-x.community/compliance-criterion/1df51e3055940bad2b48205d85db6fc7968af811ce74a9383a99dd9bef548340/data.json",
               "did:web:gaia-x.community/compliance-criterion/c754817b22a03a60efc60569ef8cf483fbf0df402d1f72c863967ab393e9b3af/data.json",
               "did:web:gaia-x.community/compliance-criterion/10e1999e6107d991f3251a57252ca95b7564d7817f34ffcdb2520a1939746749/data.json",
               "did:web:gaia-x.community/compliance-criterion/3cb671f9bb51069b27ff31208025553f6386e7d37a7429dd058a9b5924fe64fc/data.json",
               "did:web:gaia-x.community/compliance-criterion/958b517ec144386f06c238dfafa801a4e719807da0a261ecdb1546cd1246a16c/data.json",
               "did:web:gaia-x.community/compliance-criterion/5c6c37938ccec54ddbe569d1e429658d7c3a86b3cb91f5d7b9fa81b61efe6eba/data.json",
               "did:web:gaia-x.community/compliance-criterion/9975196b786b14f3992114e5e1f1cd35befdb5e7fa439e2f94bef927f328ac49/data.json",
               "did:web:gaia-x.community/compliance-criterion/18e49d1e6c56efb00f43a863c87d147e8598a450a683ab6e0db245b7d4d2af86/data.json",
               "did:web:gaia-x.community/compliance-criterion/471b984f28f8a79f7c11f3e44fe298aab7152efc5f97d29fea6d45f2e9a0efd1/data.json",
               "did:web:gaia-x.community/compliance-criterion/98d0eec88ce471c8a0485636b95b7b5f28a743131e84c79a937e8c92de7e330c/data.json",
               "did:web:gaia-x.community/compliance-criterion/c51a27275cbb87b247d21b81ef16f585d114c22e48913eb6d9fb4f0e3aa01ba6/data.json",
               "did:web:gaia-x.community/compliance-criterion/69e118feec07463d4e827aa345ee659ce32e5075e6ddcf8daff7027a99f8893c/data.json",
               "did:web:gaia-x.community/compliance-criterion/25d78bede67bb195d69b9419808d74015c3d5f4a2d733c676a7244b3950ce1b9/data.json",
               "did:web:gaia-x.community/compliance-criterion/843d0a4650499b09e37b5dd7759da4ddbc4f7d7098bc806149a8829f9ea0f315/data.json",
               "did:web:gaia-x.community/compliance-criterion/36c42f80bc75fb96d66f64f1ae4f75798c7906f2a515adfae982367d3ace56dd/data.json",
               "did:web:gaia-x.community/compliance-criterion/c9280867077e758137a1b6ce8010698e187a98e1ebf01fc28e64be76611528ad/data.json",
               "did:web:gaia-x.community/compliance-criterion/53ff2d76126a8b7404fc769734043b3a24d5e7422029cb2666f2b8a4a11fb49b/data.json",
               "did:web:gaia-x.community/compliance-criterion/12f11e3c592b4285d3f051f933a99517b997a543d5705d8717a40af6bc150b2a/data.json",
               "did:web:gaia-x.community/compliance-criterion/e06d36d7b023d7b1da6ef0c9f8d451b8fb1ddecd4538e03d9c63fa3d1c59d990/data.json",
               "did:web:gaia-x.community/compliance-criterion/74b947356bc21c2daaf7f0984b53a1493ed4678f53680d63830842bd22fe352f/data.json",
               "did:web:gaia-x.community/compliance-criterion/f36bebe520dbf1736c3840769828d093791053c5aa6c6a46d1848595e00140bc/data.json",
               "did:web:gaia-x.community/compliance-criterion/9e45260877e1c896508618cefd9f16c185e72dd89fba426eb3901eff167d2360/data.json",
               "did:web:gaia-x.community/compliance-criterion/d604a332d9ca9c8f2b93fc779b35ec2e57818fdedba5d8cef260e69af524b8f2/data.json",
               "did:web:gaia-x.community/compliance-criterion/db57c2316ae02bfb8c4b66368735234fb917995b91123775b164cbbbfa19ca6f/data.json",
               "did:web:gaia-x.community/compliance-criterion/7bedfaaa1ed74be7de48eafac21cfbad4856c424f1638d1af0a2053d2cb227a9/data.json",
               "did:web:gaia-x.community/compliance-criterion/85044fd562ef04649b9d5530e7f4ef248ae6ab81b3c9fa1f4fcd3847ce1c94f1/data.json",
               "did:web:gaia-x.community/compliance-criterion/fb797087b3951edc3add77837cd5401cacc7f2fc206413ff3c9cbff563dd8f68/data.json",
               "did:web:gaia-x.community/compliance-criterion/cde1519cd7f68cce2951987c72d75decf3626a403e89d458fbceb7ac46f06f5a/data.json",
               "did:web:gaia-x.community/compliance-criterion/c4a921a3a3df7fa678c0a521a22a5dac0528bed85233719f71c7cbf52edf302a/data.json",
               "did:web:gaia-x.community/compliance-criterion/6dcd080420ba9a1f7db82682395ed54aacdda5b6ba7c001eec61e2d19226dd2a/data.json",
               "did:web:gaia-x.community/compliance-criterion/a1f306c65a2969c1233cb0142034c0ab21dcac4615f21ad45cba9a8e4e273653/data.json",
               "did:web:gaia-x.community/compliance-criterion/273a31001e5ea054732d0983c44343612a339b6c5e9c5eb2b1209d77487ad94e/data.json",
               "did:web:gaia-x.community/compliance-criterion/1d0535639c6c2e26790ac19a790ad547ac1b4d7c12bdd0f8dd4c3f8c726fc7e5/data.json",
               "did:web:gaia-x.community/compliance-criterion/f5284af5d5bf3b1a80a90c65f65f3ff15fa15ca524085685bae85ca135d62782/data.json",
               "did:web:gaia-x.community/compliance-criterion/733aca07e2d2e11cba61dc74e387d6ca289ee895e521c9c091141be15118c5ba/data.json",
               "did:web:gaia-x.community/compliance-criterion/75571bb4c63239f0df7168e1a44ebdf278fd37738eff607cfb9049c7c2bd083b/data.json",
               "did:web:gaia-x.community/compliance-criterion/cf15736ee674b6fbe9f32bc91a9a81a2b1b1135517a352037edfb0808cddba81/data.json",
               "did:web:gaia-x.community/compliance-criterion/f223175e85cf168bd7df2858c32b5db5267772986299221f4bba5762bd8482c8/data.json",
               "did:web:gaia-x.community/compliance-criterion/3c6be9b458b4402c708733302056fd55aa58d6f1e865b61b3bf03f3db6145e47/data.json",
               "did:web:gaia-x.community/compliance-criterion/b388093c6117171072423b78a1d223caeb66d79266301e695df169d1ab391c79/data.json",
               "did:web:gaia-x.community/compliance-criterion/d6c80de92f9426e57b637f56000530e5142a085abd704e63d708ad9eb68db5dc/data.json",
               "did:web:gaia-x.community/compliance-criterion/ed6a2f7f46f6faacf8142fda2733383ae6b268ccbdadf3293fa4eeb35daf8091/data.json",
               "did:web:gaia-x.community/compliance-criterion/33ffebcc9d1c490ddee3c16f039f801a035a8fe18f88262a7fd2d6b517b4c39e/data.json",
               "did:web:gaia-x.community/compliance-criterion/26c3d521a326ee292fd20ce72f3dfffe0ffd290769283f2a2a7738e8a6847d40/data.json",
               "did:web:gaia-x.community/compliance-criterion/bcc7f7d1ca3d08cc612e3fb3aacc4e3e9e57fd4f15fd6610fc1027f3cfd7ab10/data.json",
               "did:web:gaia-x.community/compliance-criterion/dfb922e542fe48da698dfd1599053f203311ca650003a786e88cc392ae54b698/data.json",
               "did:web:gaia-x.community/compliance-criterion/2808237322eab75f3d041469954dc61bcaab3153b63558d7f8f4449c4bfcc894/data.json",
               "did:web:gaia-x.community/compliance-criterion/bc3a08b25f3e84f6e0bbc06e03b038cc04c9d3eee2a654d956e98f6360e6baa0/data.json"
            ]
         },
         "issuanceDate":"2023-03-23T13:37:05.126757+00:00",
         "proof":{
            "type":"JsonWebSignature2020",
            "proofPurpose":"assertionMethod",
            "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
            "created":"2023-03-23T13:37:05.126757+00:00",
            "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..K_IPBOEMUiClEjdCzaR0F83C_pSGRR24fNIT69-ZlBbuyzhutZiFTRBwd7zBUmICi_d8xMf3wp8aQQcypfekCU1uUXoGtlrxuc3cvhh4tO38_95zCiOEdyJ8_ZMv2QfyyxCv6OCH-AtSzkKVQ0Gdx_r7myVnN6mP5oftyDId0QJM6_GjRdyQxUM5UXZofHZ9b-OAyGfiM9R1ayBf7r2lwYNZNd8vmN67O_AFMnEkTgG9DhG42TwdVvpgyD002TaXrT8fo05ISx5cSGP5KVJ0yqe6eDx0k8e3OIlyrIC8R-wy6C9NJOkn_g1LJ0-vCOfJvc9rHvAg74QDkHKcW_yKkw"
         }
      },
      {
         "@context":[
            "https://www.w3.org/2018/credentials/v1"
         ],
         "type":[
            "VerifiableCredential",
            "ServiceOfferingCredentialExperimental"
         ],
         "id":"https://catalogue.gaia-x.eu/credentials/ServiceOfferingCredentialExperimental/1679579636460",
         "issuer":"did:web:compliance.abc-federation.gaia-x.community",
         "issuanceDate":"2023-03-23T13:53:56.460Z",
         "expirationDate":"2023-06-21T13:53:56.460Z",
         "credentialSubject":{
            "id":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
            "hash":"3e30bb6a868f7efa53498024092027d1799c1b435b007afbf4ab9a0b4646a595"
         },
         "proof":{
            "type":"JsonWebSignature2020",
            "created":"2023-03-23T13:53:56.460Z",
            "proofPurpose":"assertionMethod",
            "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..Br8F-jZ6p55gQwzzi_r-o-rZEcuDCxE8q2mggHkAznLuoF7Tay_ySONKhVnwfLyPEPkzOxT1EAyiXTGSYsoYQ1ertWNhIFrjECuUofuoElySAYjYJ1C7rcZCKfhT9jx4npNEcJTNEfiGk_G9rdpG9qIqIzSndHAeIpdFs2SmNObLnPUzY-FMQ5aTwNZGjtveFuojln7mER4blkviV-StHidoGLwnzRKiARVnX2_mKQo2CZ0-NLHl6tAqKKvrgRZ0CugbRIP2pxQIkG08Uazj67ZdR-wcblGI89lXWBpBv1alFGYTUn6uhXyltdHt5P3-00XcPc8SGdB31s8tINOtlw",
            "verificationMethod":"did:web:compliance.abc-federation.gaia-x.community"
         }
      },
      {
         "@context":[
            "https://www.w3.org/2018/credentials/v1"
         ],
         "id":"https://labelling.abc-federation.gaia-x.community/vc/8a6d70de-c995-40d8-a2af-a7bc98844ef5",
         "type":[
            "VerifiableCredential",
            "GrantedLabel"
         ],
         "credentialSubject":{
            "id":"did:web:ovhcloud.provider.gaia-x.community:participant:3f9227f3-3ad0-447d-a875-25395453c73e/granted-label/8a6d70de-c995-40d8-a2af-a7bc98844ef5/data.json",
            "type":[
               "GrantedLabel"
            ],
            "vc":"did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/5fa3c93afd847bced57bdee6c9b3387bb335d3cab4a5cb1a96d02c67f1aca34f/data.json",
            "label":{
               "hasName":"Gaia-X label Level 1",
               "id":"did:web:gaia-x.community:participant:020dc2af9516b2af509e349f577c06aa681ef9799810faf79bb27de791af0e0d/vc/ee73e5504ca6d9e79fe7726fc31eca9b609f45ed761debfee60cca9257b9d0ec/data.json"
            },
            "proof-vc":{
               "type":"JsonWebSignature2020",
               "proofPurpose":"assertionMethod",
               "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
               "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..BtcnneOWDOgKVVywlBBd4RnDh3gSn17lw53bUt4wHPVLELdkjvVj2SPSVXbkCOxuTKuO1O2nimTTkMBR5skCM_gKQscr1TX24jbPgEdrdOLa0o1_AsiilGNgDlDQvAXQI7MRCQPrHsTai5Ur8wOfeGwJDopD9t4FDMmz-wtf5RK8vJRIlaUkXqX1aPovdmdHcNsAnhFgRHt6mf0qMZfDbv0rHiTqf4G5sMtvrPeGy55HAQvGje3-A38nnGN4F46MjW9sxKXuXTJF9JML6WHCnlwRAUzgCgJ23MAsO4gEYAaQ3W-fd4RnJNUV2BWMqxV-EUbOKMy5TbUTR1tsobXt2g"
            }
         },
         "expirationDate":"2023-06-21T00:00:00.000Z",
         "issuer":"did:web:abc-federation.gaia-x.community",
         "issuanceDate":"2023-03-23T13:56:02.464898+00:00",
         "proof":{
            "type":"JsonWebSignature2020",
            "proofPurpose":"assertionMethod",
            "verificationMethod":"did:web:abc-federation.gaia-x.community",
            "created":"2023-03-23T13:56:02.464898+00:00",
            "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..JAdJ7Rc2o4EUSAzHdgJmta5IC2KfOKrMO1NDDVPRLjJvkpxl7c13RpKaYLJ5RR3EZPI8JCkVEZf2ILgWOOOg4J7EDX-I9ne7R8AQGWcpEo1JRvCFTRm3FQoe6BUVhd6QVq5vQKe03PpVTp80246Kth8TyIrScsGGlj15aqa2XmyqcqaUl7rbjIqE8ve-T8SIsrQlhvEVxFlvBkbQjXME2juXefF3A1mNnPcKDiGRJme4Wwn5E91BSa7k5z33jw0DnA_-Q0i-BIsLNcAwFQCs7-t8W6_RK6urN1AA5gHH4DpTW8Ru3t7gPtHAL4aOuluDNsiEM-LQcfG4XHPiirb76w"
         }
      }
   ],
   "proof":{
      "type":"JsonWebSignature2020",
      "proofPurpose":"assertionMethod",
      "verificationMethod":"did:web:dufourstorage.provider.gaia-x.community",
      "jws":"eyJhbGciOiJQUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..SOVoW6IUMsLGa41nOI31qUeAN0PccXOiA3RQQD_C9sI1Kz9seHssBoXVRPQeE4FIBXJq6I3rL31t62FdYjGqpJGZskGs03XpV5mDbyi_ukpsZJ1ZyLUt6HkQIwNlxF2vCswmB22QzO4XdVyjCYQUJUCED4Rh_eQHw6Wd88OTfIFqfDVcvdFpLiJhaiATCe4AUhIuISlGmCUFNRb3EuqtVHPFTEB8dqWYybXJnI3CmtpLRFJmW1NbYbG4TQZ1a_ibarM53D3iX_B0SO-yiRj6oRdf-GtndEDwJNuyEXj5G1bJWkc6AVK7xabfJ82SsYAdEhWlsHbI-U-JxQQP94cv3g"
   }
}
```

## Step04

### s4 - Participant Mont Blanc IOT - Claims from Schema

### s4 - Participant Mont Blanc IOT - Set of claims

### s4 - Participant Mont Blanc IOT - Signed VC

### s4 - Participant Mont Blanc IOT - Compliance Certification Credential

### s4 - Participant Mont Blanc IOT - Participant Self-Description (end of S1)

### s4 - Participant Alice - Claims from Schema

### s4 - Participant Alice - Set of claims

### s4 - Participant Alice - Signed VC

### s4 - Participant Alice - Signed VP (Person + LegalPerson)


## Step05

https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/-/blob/main/code/tmp/generated_catalog.json

### s5 - Set of Service Offering SD

Catalogue OVH https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/common-federation-service-catalogue/-/blob/main/code/service_catalogue/graphdb/examples/catalog.json

https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/schema-provider/-/blob/develop/federated-catalogue/jsonld-examples/catalog.json 

### s5 - Filtered SubSet of Service Offering SD

### s5 - Service Offering SD ID

### s5 - Service Offering SD Description with Issuers Trust results


## Step06

### s6 - Participant Alice - Signed VP (Person + LegalPerson)

### s6 - Service Instance - rules of services

### s6 - Auth token JWT

### s6 - Alice file to be pushed
