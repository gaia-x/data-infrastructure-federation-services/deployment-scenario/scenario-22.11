# Functional Blocks schema

```mermaid
flowchart TB
    subgraph Federated Services
        subgraph Trust Framework Module
            subgraph Registry Service
                r1("Trusted Registry Trust Anchor Service")
                r2("Federated Catalogue")
                r3("Schema Provider")
                r4("Policy Rules Provider")
            end
            subgraph Compliance Service
                c1("Schema Validator")
                c2("Policy Rules Checker Service")
                c3("VC Issuer")   
            end
            subgraph Labelling Service
                l1("Schema Validator")
                l2("Rules Checker Service")
                l3("VC Issuer")   
            end
        end
        subgraph Federation Web Portal
            subgraph "Federation Catalogue"
                cat8("Common Federation Service Catalogue")
            end
        end
        subgraph Toolbox
            tb1("Service Description Wizard Tool")
            tb2("DID Universal Resolver")
            tb3("Notarization Service")
        end
    end
```
```mermaid
flowchart TB
    subgraph Organization landscape
        subgraph Organization
            p1("VC Issuer")
            p3("Credential Manager Wallet")
        end
        subgraph Service Instances
            subgraph Policy Decision Point
                c4("Rules Checker")
                br("Business data referential")
                sr("Service policy rules")
            end
            r12("Running Service")
        end
    end
```


# Functional Blocks Explanations

## Trust Framework module

Ref Specification: Trust framework 22.04 https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.04/
Trust framework in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#gaia-x-trust-framework
Self-description Verification Process in TAD 5.6.2: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/self-description/
Self-description compliance in TAD 6.5.2: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#self-description-compliance

The Trust Framework module is the set of components in charge of establishing conformity and trust between actors inside a Federation. The different components of the Trust Framework module are operated by a Federator inside a Federation.

We distinguish two subset of components, the Compliance part and the Registry part.

### Registry Service

Existing component: Gaia-X Lab https://gitlab.com/gaia-x/lab/compliance/gx-registry
reference in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#gaia-x-registry

The Gaia-X Registry is the backbone of the ecosystem governance which stores information. It is a public distributed, non-repudiable, immutable, permissionless database with a decentralized infrastructure and the capacity to automate code execution. (according to Architecture Document).

We are going to use this service to store all data we need to run a [Gaia-X ecosystem](https://gaia-x.gitlab.io/technical-committee/glossary/ecosystem/).

### Policy Rules Provider

A registry in which the authorization rules (Rego files) are stored in a versioned manner and under the exclusive control of a trusted authority.
It can be a simple Git repository.

#### Trusted Registry Trust Anchor Service

The Trust Anchor Service stores and exposes Trust Anchor entities. For a given ecosystem, the Trust anchors are the entities considered by all Participants to be trustworthy when establishing the chain of cryptographic certificates.

Registry API developed by Gaia-X Lab is able to manage Trust Anchor ([Registry API Swagger](https://registry.gaia-x.eu/docs/#/)).

Trust Anchor in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors 

#### Federated Catalogue

ref in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/federation_service/#federated-catalogue

Existing component: https://gitlab.com/gaia-x/data-infrastructure-federation-services/cat

Federated Catalogue stores service offering entities exposed by Participants Provider. With Federated Catalogue, Consumer is able to search the most appropriate service according to the searched labels.

#### Schema Provider

To validate request format, we need to check if the request is compliant with expected format which is a schema. Expected schema are stored in schema provider.

There is Gaia-X expected schema which are mandatories to be Gaia-X compliant.


### Compliance Service / Policy Rule Checker (PCR)

Existing component: Gaia-X Lab  https://gitlab.com/gaia-x/lab/compliance/gx-compliance

Gaia-X Compliance Service / PCR deliver a Gaia-x label to a Gaia-x Service Offering. 
The Gaia-x Compliance service / PCR perform checks : 
* syntactic correctness.
* schema validity.
* cryptographic signature validation.
* attribute value consistency.
* attribute value verification.

The Gaia-x Compliance service / PCR sends informations to VC Issuer to generate a valid Verifiable Credential signed. 

### Schema Validator

Ensure a given Self Description body follows a well known schema defined in the Schema Provider. For example, a Self Description for a Participant must match the Participant JSON-LD or Participant SHACL file inside the Schema provider.

Current schemas can be found here : https://gaia-x.gitlab.io/technical-committee/service-characteristics/widoco/core/core.html


### Policy Rule Checker

perform all the needed verification (signature, consistency, verification) described in a policy rule description file 

Existing component (some part): https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa

reference: Policy Rules Document  https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X_Policy-Rules_Document_v22.04_Final.pdf

In addition to being in the correct format, requests must also validate functional rules. For example, If legalAddress.country is located in United States of America, than a valid legalAddress.state using the two-letter state abbreviations is mandatory.

This rules are stored inside Policy Rule Provider.

Some rules allow the assignment of a label. The rule provider is also responsible for returning the label associated with the rule if the rule is verified.

### VC Issuer

Tool to issue a VC.


### Labelling service

Rule engine computing Self Descriptions to assign a Gaia-x label according to policy rules.
The Gaia-x Compliance Service / PCR can be linked to the Label Rule Assignment component to get a label regarding claims provided. 
Gaia-X Label in TAD: https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#gaia-x-labels
Gaia-x Labels 
Reference for the criteria: Gaia-X Labelling Criteria https://gaia-x.eu/wp-content/uploads/2022/05/Gaia-X-labelling-criteria-v22.04_Final.pdf

## Federation Web Portal

A web portal to interact with federation services (can interact with User Agent API).

##Toolbox

### DID Universal resolver

A DID universal resolver is available at : http://resolver.lab.gaia-x.eu:8080.  
See : https://gitlab.com/gaia-x/technical-committee/federation-services/iam/-/issues/9 for more information.

### Service Description Wizard Tool

A tool to help filling in and managing Service Descriptions (with GUI)

#### Notarization Service

Existing component: https://gitlab.com/gaia-x/data-infrastructure-federation-services/not 

The Notarization service is in charge of issuing a VC signed by a Third-Party or a CAB to digitalized as a proof a document that could not yet be available as a credential. The Notary must be trusted by the all actors of the federation. 

## Organization

### Credential Manager Wallet

Tool were the organisation stores its credentials.
Existing component: 
OCM (Organization Credential Manager)
https://gitlab.com/gaia-x/data-infrastructure-federation-services/ocm

https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/federation_service/#identity-and-access-management

OCM allow to store and retrieve VP as provided by the supplier and signed by the supplier. 

### DID Storage Point



### Policy Decision Point

The Policy Decision Point represent the [PDP](https://csrc.nist.gov/glossary/term/policy_decision_point) (Policy Decision Point).

A **PDP** take three inputs :

```mermaid
flowchart TD

sr("Service policy rules")
br[("Business data referential")]
ua("User attributes\n(from VC)")
pdp{"PDP\n(policy engine)"}
d(["Decision\nauthorized or not"])
sr-->pdp
br-->pdp
ua-->pdp
pdp-->d
```

* **Service Policy Rules**: A set of rules, contextualized for the business scope of the service that determine why a user action is allowed or not. These rules are expressed in [Rego](https://www.openpolicyagent.org/docs/latest/policy-language/) of the [OPA](https://www.openpolicyagent.org) project. This set of rules can be retrieved from the authorization registry.
  * Exemple (in natural language): allow `user_id` to `create a volume` if `current usage` + `desired size of new volume` < `user quota`. 
* **Business data repository**: Is a local (service) data repository (typically a database) that contains per-user states for that service.
  * In our example: the `current usage` per `user_id`.
* **User attributes**: are extracted from the verifiable presentation at each request and contain information useful for making the authorization decision.
  * In our example: the `user quota` and the `user_id`.

In the context of Gaia-X the technology selected to manage PDP is [OPA/Rego](https://www.openpolicyagent.org).

The [Open Policy Agent] technology (https://www.openpolicyagent.org) can be used in two modes:

* **Daemon**: In a separate process that can act as a network proxy that evaluates the policy and relays the request if authorized.
* **Library**: Integrate OPA as a library into the backend service that evaluates the policy.

The original library supported by the project is available for Go, however there are a [multitude](https://www.openpolicyagent.org/docs/v0.15.1/ecosystem/#) of compatible environments and applications today.

The recommendation of the IAM team is to place the policy engine as close as possible to the business code, and therefore to favor the library approach. 

This avoids an additional transit of the request through the network but above all avoids having a service that accepts any request indiscriminately.


### Business data referential
The place were the organisation stores the business data needed to perform the rules

### Service Policy Rules

The rules that describe the usage of the service, according to the contract signed by the two parties

### Running Service

The actual service provided by the provider to the consumer

## User Agent

User Agent is the entry point for participant to interact with Federation services. His role is to orchestrate interactions between the different federation services.

Technically the User Agent can be an API or a SDK or a CLI.


# Components usage

## Step 01 - Onboard company B as a Provider

```mermaid
sequenceDiagram
    actor Bob
    participant User Agent
    participant Compliance
    participant Schema Provider as [Registry]<br>Schema Provider
    participant Policy Rule Provider as [Registry]<br> Policy Rule Provider
    participant Trusted Registry as [Registry]<br> Trusted Registry
    participant Schema Validator as [Compliance Service / PCR]<br>Schema Validator
    participant Policy Rule Checker as [Compliance Service / PCR]<br>Rule Checker
    participant Compliance VC Issuer as [Compliance Service / PCR]<br>VC Issuer
    participant Provider VC Issuer as [Provider]<br>VC Issuer

    Bob->>+User Agent:Request Participant Schema
    User Agent->>+Schema Provider: Request Participant Schema
    Schema Provider-->>-User Agent: Participant schema
    User Agent-->>-Bob: Participant schema
    Bob->>Bob: Fill-in Participant-claims, collect set of claims
    Note right of Bob: Set of claims
    Bob->>+User Agent:Request Participant-VC
    User Agent->>+Provider VC Issuer: Request Participant-VC
    Provider VC Issuer-->>-User Agent: Issue Participant-VC
    User Agent-->>-Bob: Participant-VC
    Note right of Bob: Participant VC
    Bob->>+User Agent:Request Compliance VC for Participant-VC
    User Agent ->>+Compliance:Request Compliance VC for Participant-VC
    Compliance->>+Schema Validator: Validate Participant-VC
    Schema Validator->>+Schema Provider: Request Participant Schema
    Schema Provider-->>-Schema Validator: Participant Schema
    Schema Validator-->>-Compliance: Participant Schema OK
    Compliance->>+Policy Rule Checker: Evaluate rules for Participant-VC
    Policy Rule Checker->>+Policy Rule Provider: Request rules for Participant
    Policy Rule Provider-->>-Policy Rule Checker: Applicable Rules for a Participant-VC
    Policy Rule Checker->>+Trusted Registry: Check if all issuers are trusted
    Trusted Registry-->>-Policy Rule Checker: Issuers are trusted
    Policy Rule Checker-->>-Compliance: Participant-VC is OK according to rules
    Compliance->>+Compliance VC Issuer: Request Compliance-VC generation
    Compliance VC Issuer-->>-Compliance: Issue Compliance-VC
    Compliance-->>-User Agent: Issue Compliance-VC
    User Agent-->>-Bob: Compliance-VC
    Note right of Bob: ComplianceCertificateCredential for <br> complianceReference object <br>Gaia-X Compliance
    Bob->>Bob: Expose the SD (didweb)
```

## Step 02 - Add data storage service offering to the federated catalogue

### Step 02a - Build the Service Offering VP
```mermaid
sequenceDiagram
    actor Bob
    participant User Agent
    participant Compliance
    participant Schema Provider as [Registry]<br>Schema Provider
    participant Provider VC Issuer as [Provider]<br>VC Issuer
    participant Federation Notarization Service as [Federation] <br> Notarization Service
    participant Federation VC Issuer as [Federation]<br>VC Issuer
    
    Note right of Bob: Company B exists as a Participant-Provider
    Bob->>+User Agent:Request Service Offering Schema
    User Agent->>+Schema Provider: Request Service Offering Schema
    Schema Provider-->>-User Agent: Service Offering schema
    User Agent-->>-Bob: Service Offering schema
    Bob->>Bob: Fill-in Service Offering schema
    Bob->>Bob: Service Offering Claim
    Bob->>+User Agent:Request Service Offering-VC
    User Agent->>+Provider VC Issuer: Request Service Offering-VC
    Provider VC Issuer-->>-User Agent: Issue Service Offering-VC
    User Agent-->>-Bob: Service Offering-VC
    Note right of Bob: Signed Service Offering-VC
    opt For each needed certification document that is not yet a VC
        Note right of Bob: owns PDF as proof for referenced certification 
        Bob->>+User Agent:Request VC for a certification Reference
        User Agent->>+Federation Notarization Service: Request VC for a certification Reference
        Federation Notarization Service->>Federation Notarization Service: Check document
        Federation Notarization Service->>+Federation VC Issuer: Request Signature for a certification Reference
        Federation VC Issuer-->>-Federation Notarization Service: Request Signature for a certification Reference
        Federation Notarization Service-->>-User Agent: Return VC for a certification Reference
        User Agent-->>-Bob: VC for certification Reference
        Note right of Bob: Self-signed ThirdPartyComplianceCertificateCredential for complianceReference object (ISO27001)
    end
    Bob->>Bob:  collect set of claims (certifications+serviceoffering)
    Note right of Bob: Set of claims
    Bob->>+User Agent:Request VP for a set of claims
    User Agent->>+Provider VC Issuer: Request Service Offering-VP
    Provider VC Issuer-->>-User Agent: Issue Service Offering-VP
    User Agent-->>-Bob:Return a VP for a set of claims
    Note right of Bob: Signed Service Offering-VP

```

### Step 02b - Compliance and Catalogue


```mermaid
sequenceDiagram
    actor Bob
    participant User Agent
    participant Compliance
    participant Schema Provider as [Registry]<br>Schema Provider
    participant Policy Rule Provider as [Registry]<br> Policy Rule Provider
    participant Trusted Registry as [Registry]<br> Trusted Registry
    participant Schema Validator as [Compliance Service / PCR]<br>Schema Validator
    participant Policy Rule Checker as [Compliance Service / PCR]<br>Rule Checker
    participant Compliance VC Issuer as [Compliance Service / PCR]<br>VC Issuer
    participant Federated Catalogue

    Note right of Bob: Signed Service Offering-VP
    Bob->>+User Agent:Request Compliance VC for Service Offering-VP
    User Agent ->>+Compliance:Request Compliance VC for Service Offering-VP
    Compliance->>+Schema Validator: Validate Service Offering-VP
    Schema Validator->>+Schema Provider: Request Service Offering Schema
    Schema Provider-->>-Schema Validator: Service Offering Schema
    Schema Validator-->>-Compliance: Service Offering Schema OK
    Compliance->>+Policy Rule Checker: Evaluate rules for Service Offering-VC
    Policy Rule Checker->>+Policy Rule Provider: Request rules for Service Offering
    Policy Rule Provider-->>-Policy Rule Checker: Applicable Rules for a Service Offering-VC
    Policy Rule Checker->>+Trusted Registry: Check if all issuers are trusted
    Trusted Registry-->>-Policy Rule Checker: Issuers are trusted
    Policy Rule Checker-->>-Compliance: return a Compliance-VC (if ok with rules)
    Note right of Compliance: Not-Signed Compliance-VC
    Compliance->>+Compliance VC Issuer: Request Compliance-VC generation
    Compliance VC Issuer-->>-Compliance: Issue Compliance-VC
    Note right of Compliance: Signed Compliance-VC
    Compliance-->>-User Agent: return Signed Compliance-VC
    User Agent-->>-Bob: Compliance-VC
    Note right of Bob: ComplianceCertificateCredential for complianceReference object Gaia-X Compliance
    Bob->>Bob: add the VC to the ServiceOffering VC
    Bob->>Bob: Expose the SD (didweb)
    Bob->>+Federated Catalogue: Register Service Offering
    Federated Catalogue->>+Trusted Registry: Check if all issuers are trusted
    Trusted Registry-->>-Federated Catalogue: Issuers are trusted
    Federated Catalogue-->>-Bob: Ok
    
```

## Step 03 - Define GAIA-X level x label to the service

```mermaid
sequenceDiagram
    actor Bob
    participant User Agent
    Note right of User Agent: Authenticate and Authorize
    participant Label Issuer as [Compliance Service / PCR]<br>Label Issuer
    participant Policy Rule Checker as [Compliance Service / PCR]<br>Policy Rule Checker
    participant Policy Rule Provider as [Registry]<br> Policy Rule Provider
    participant Trusted Registry as [Registry]<br> Trusted Registry
    participant Schema Validator as [Compliance Service / PCR]<br>Schema Validator
    participant Schema Provider as [Registry]<br>Schema Provider
    participant Label VC Issuer as [Compliance Service / PCR]<br>VC Issuer
    participant Federated Catalogue

    Bob->>Bob: Write a self signed VC with Service Offering SD + additional Compliance Reference (self-signed or issued by a trusted CAB)
    Note right of Bob: Service Offering SD from end of step02
    Bob->>+User Agent: Request Label for Service Offering
    User Agent ->>+Label Issuer:Request Label for Service Offering
    Label Issuer->>+Schema Validator: Validate Participant-VC
    Schema Validator->>+Schema Provider: Request Participant Schema
    Schema Provider-->>-Schema Validator: Participant Schema
    Schema Validator-->>-Label Issuer: Participant Schema OK
    Label Issuer->>+Policy Rule Checker: Validate claim
    Policy Rule Checker->>+Policy Rule Provider: Request rules for Label
    Policy Rule Provider-->>-Policy Rule Checker: Applicable Rules for a Label
    Policy Rule Checker->>+Trusted Registry: Check if all issuers are trusted
    Trusted Registry-->>-Policy Rule Checker: Issuers are trusted
    Policy Rule Checker->>Policy Rule Checker: Check ComplianceReferenceCredential validity
    Policy Rule Checker-->>-Label Issuer:  send back label level according to criteria rules
    Label Issuer->>+Label VC Issuer: Request Label-VC generation
    Label VC Issuer-->>-Label Issuer: Issue Label-VC
    Label Issuer-->>-User Agent: GAIA-X label for the Service Offering
    User Agent-->>-Bob: Label VC for Service Offering
    Note right of Bob: ComplianceCertificateCredential for complianceReference object Gaia-X Label
    Bob->>Bob: Update Service Offering SD with label information
    Note right of Bob:  Service Offering SD with label
    Bob->>+Federated Catalogue: Update Service Offering SD with label information
    Federated Catalogue->>+Trusted Registry: Check if all issuers are trusted
    Trusted Registry-->>-Federated Catalogue: Issuers are trusted
    Federated Catalogue-->>-Bob: Ok
```

## Step 04 - Onboard company A as a consumer, create VC for Alice
```mermaid
%%Step 04 - Onboard company A as a consumer, create VC for Alice%%
sequenceDiagram
    actor Mary as Mary <br/> <<actor admin>>
    actor Alice as Alice <br/> <<actor>>
    participant ocm as [Organization] <br/> Organization Wallet
    participant pcm as [Alice] <br/> Alice Wallet
    participant sp as Schema Provider
    participant vci as [Organization] <br/> VC Issuer of Company A

    Note left of ocm: Reproduce all steps from step01 <br>to get a Participant SD for Company A <br> with Consumer Schema
    Note left of Alice: Alice to OCM or Alice to Mary?
    Alice->>+ocm: Request VC as CompanyA Employee
    
    ocm->>Mary:check setup for Alice as CompanyA Employee
    Mary->>ocm:Validate setup for Alice as CompanyA Employee
    Note left of ocm: do we switch to manual?
    ocm->>+sp: Request Schema Consumer
    sp-->>-ocm: Consumer Schema

    ocm->>vci: request Alice Consumer VC generation
    vci->>ocm: Issue Alice Consumer VC

    ocm-->>-Alice: signed VC 
    Alice->>pcm: Store her VC in her personal Credential Manager Wallet

```

## Step 05 - Search services with labels

```mermaid
%%Step 05 - Search services with labels%%
sequenceDiagram
    actor alice as Alice <br/> <<actor>>
    participant fa as User Agent
    participant fc as Federated Catalogue
    participant ac as All Registered Catalogues
    alice->>+fa:Request full list of services
    fa->>+fc: Ask the whole catalogue
    loop For all entries
        fc->>+ac: collect information on all services
        ac-->>-fc: service Offering SDs
    end
    fc-->>-fa: the whole catalogue
    fa-->-alice: full list of services
    Note right of alice: Set of Service Offering SD
    alice->>alice: Create search request
    alice->>+fa: Search with filter parameters
    fa->>+fc: Apply filters
    fc-->>-fa: filtered result
    fa-->-alice: filtered result
    Note right of alice: Set of Service Offering SD
    alice->>+fa: Ask for one service offering Issuers validity
    Note right of alice: Service Offering SD 
    fa->>+fc: Ask for the SD Issuers validity
    fc->>+Trusted Registry: Check if all issuers are trusted
    Trusted Registry-->>-fc: Issuers trust status
    fc-->+fa: return All issuers status + service information
    fa-->>-alice: display results
    Note right of alice: Service Offering description <br>with Issuers Trust results
```

## Step 06 - Consume a service
```mermaid
%%Step 06 - Consume a service%%
sequenceDiagram
    actor alice as Alice <br/> <<actor>>
    participant pa as Participant Agent
    participant sic as [Company A] Service Instance Trust Store <br/> (Active contracts)
    participant si as Service Instance of Company B
    participant PDP as [Service Instance] <br/>PDP - Policy Rule Checker
    participant Service Policy Rule Provider as [Service Instance]<br> Service Policy Rule Provider
    participant Business Data Referential as [Service Instance]<br> Business Data referential
    participant Service
    
    opt in or out of scenario
        alice->>+pa: Authenticates
        pa-->-alice: OK
        alice->>+pa: request service instances for Alice
        pa->>+sic:request service instances for Alice
        sic->>sic: Check all Credentials validity
        sic->>+pa: return list of active service instance
        pa-->-alice: return list of active service instance
    end

    Note right of alice: Need an exchange with si <br> to get list of information to be included in the VP
    alice->>alice: create VP
    alice->>+si: Authenticates with VP to consume
    si->>+PDP: Request authorization
    PDP->>+Service Policy Rule Provider: Request rules for Services (contract)
    Service Policy Rule Provider-->>-PDP: Applicable Rules for the service
    PDP->>+Business Data Referential: Request Business Data (contract)
    Business Data Referential-->>-PDP: Applicable Business Data for the service
    PDP->>PDP: Check Credential validity
    PDP->>PDP: All rules and VP are OK
    PDP-->>-si:  Authorize
    si->>+Service: send request
    Service-->-si: return output (consume)
    si-->-alice: return output (consume)
```
