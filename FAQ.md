# FAQ

List of open questions to be resolved now or later :-)

## How does the VC-Issuer and Notarization services interact together?

Each participant who will need to **create** SelfDescriptions VC must run a Notarization service.

Notarization is the public endpoint. Internally, it calls the vc-issuer which is the only component able to access cyrptographic keys.
For the demo, Notarization can be a pass through to vc-issuer but in real life, it should manage a kind of authentication/authorization of requests.

## How are defined the values of `issuer` and `id` fields by the VC-Issuer?

The `issuer` field must be equals to the `verificationMethod` asserted by the VC-issuer in the `proof` attribute. 

Concretely, a VC issued by `company A` Notarization/vc-issuer indicates company A in the verificationMethod and issuer.

The `id` field must be a resolvable IRI and so it is the responsibility of the participant to define it then expose the VC or not.

## What is the role of the federation level `compliance` service regarding the gaia-x `compliance` level?

Gaia-X level compliance service (https://compliance.gaia-x.eu/v2206/)[https://compliance.gaia-x.eu/v2206/] assess Gaia-X level compliancy.
Federation level compliance service (https://compliance.abc-federation.gxfs.fr/)[https://compliance.abc-federation.gxfs.fr/] assess federation specific compliancy. 

Participants in ABC federation must define common compliance rules. In particular, if Gaia-X level compliance is enough or not and for given SelfDescriptions types.

**Question: what is the integration between Federation compliance and Gaia-X compliance? (Pass thru, independant systems, ...)**
